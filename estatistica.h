#ifndef ESTATISTICA_H_INCLUDED
#define ESTATISTICA_H_INCLUDED
#include "camada1.h"

void calcularDistanciaMediaPercorridaPorCadaBicicleta(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, tipoBicicleta Bicicletas[], int quantidadeBicicletas);
void mostrarDistanciaMediaPercorridaPorBicicleta(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, tipoBicicleta Bicicletas[], int quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS], char estado[][MAX_STRING_GLOBAIS]);
int emprestimosEntreData(tipoData inicio, tipoData fim, tipoEmprestimo Emprestimos[], int quantidadeEmprestimos);
float *percentagemEmprestimosPorTipo(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, int quantidadeTipos, tipoUtente Utentes[], int quantidadeUtentes);
void mostrarPercentagensPorTipo(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, tipoUtente Utentes[], int quantidadeUtentes, char utente_tipo[][MAX_STRING_GLOBAIS], int quantidadeTipos);
void ordenaUtentesPorMaisEmprestimosDecres(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, tipoUtente Utentes[], int quantidadeUtentes, char utente_tipo[][MAX_STRING_GLOBAIS]);
void mostrarUtentesQuantEmprestimosDecres(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, tipoUtente Utentes[], int quantidadeUtentes, char utente_tipo[][MAX_STRING_GLOBAIS]);
void mostraCampusComMaiorQuantiadeDeEmprestimos(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, char localizacao[][MAX_STRING_GLOBAIS]);
int quantidadeUtentesPorBicicleta(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, char designacao[MAX_STRING_DESIGNACAO]);
void mostrarNumeroDeUtentesQueUtilizaramUmaBicicleta(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, tipoBicicleta Bicicletas[], int quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS], char estado_bicicleta[][MAX_STRING_GLOBAIS]);
void quantidadeDeEmprestimosEntreDuasDatas(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos);
void ultimaBicicleta_MostraEmprestimos(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, tipoUtente Utentes[], int quantidadeUtentes, tipoBicicleta Bicicletas[], int quantidadeBicicletas, char utente_tipo[][MAX_STRING_GLOBAIS], char localizacao[][MAX_STRING_GLOBAIS], char estado_bicicleta[][MAX_STRING_GLOBAIS]);

#endif /* ESTATISTICA_H_INCLUDED */
