#ifndef AVARIAS_H_INCLUDED
#define AVARIAS_H_INCLUDED
#include "camada1.h"

void reportAvaria(tipoUtente *utente, char ficheiro_avarias[MAX_STRING], int *quantidadeAvarias, tipoEmprestimo *Emprestimos[], int *quantidadeEmprestimos, tipoBicicleta Bicicletas[], int quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS], char estado[][MAX_STRING_GLOBAIS]);
void criarAvaria(char ficheiroLogAvarias[MAX_STRING], char designacao[MAX_STRING_DESIGNACAO]);
int logReportAvaria(char ficheiroLogAvarias[MAX_STRING], tipoReportAvaria avaria);
void repararBicicleta(tipoBicicleta Bicicletas[], int quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS], char estado[][MAX_STRING_GLOBAIS]);

#endif /* AVARIAS_H_INCLUDED */
