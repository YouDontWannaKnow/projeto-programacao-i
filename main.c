/*
    Grupo PL1PL_G11
    Bruno Santos N�2171568
    Marco Oliveira N�2182165
*/

#include "camada1.h"

void cabecalho(int autosave, int quantidadeBicicletas, int bicicletasDisponiveis, int quantidadeEmprestimos, int quantidadeListaDeEspera, int quantidadeUtentes, int distanciaPercorridaTotal, int quantidadeAvarias);
char menuPrincipal();
char menuAdmin(int bicicletasAvariadas);
char menuCriterioDeAtribuicao();
char menuUser(tipoUtente utente, char utente_tipo[][MAX_STRING_GLOBAIS], int quantidadeListaDeEspera);
char menuEstatistica();
char menuVar_Add_Upd_Rem(int quant, char var[MAX_STRING]);


int main()
{
    char localizacao[4][MAX_STRING_GLOBAIS] = {
        {'R', 'e', 's', 'i', 'd', 'e', 'n', 'c', 'i', 'a', 's', '\0'},
        {'C', 'a', 'm', 'p', 'u', 's', ' ', '1', '\0'},
        {'C', 'a', 'm', 'p', 'u', 's', ' ', '2', '\0'},
        {'C', 'a', 'm', 'p', 'u', 's', ' ', '5', '\0'}
        },
        estado_bicicleta[3][MAX_STRING_GLOBAIS] = {
        {'D', 'i', 's', 'p', 'o', 'n', 'i', 'v', 'e', 'l', '\0'},
        {'E', 'm', 'p', 'r', 'e', 's', 't', 'a', 'd', 'a', '\0'},
        {'A', 'v', 'a', 'r', 'i', 'a', 'd', 'a', '\0' }
        },
        utente_tipo[4][MAX_STRING_GLOBAIS] = {
        { 'e','s','t','u','d','a','n','t','e','\0' },
        { 'd', 'o', 'c', 'e', 'n', 't', 'e', '\0' },
        { 't', 'e', 'c', 'n', 'i', 'c', 'o', ' ', 'a', 'd', 'm', 'i', 'n', 'i', 's', 't', 'r', 'a', 't', 'i', 'v', 'o', '\0' },
        { 'c', 'o', 'n', 'v', 'i', 'd', 'a', 'd', 'o', '\0' }
    };

    char ficheiro_avarias[MAX_STRING_GLOBAIS] = { 'A', 'v', 'a', 'r', 'i', 'a', 's', '.', 'l', 'o', 'g', '\0'};

    tipoBicicleta Bicicletas[MAX_BICICLETA];
    int quantidadeBicicletas=0, quantidadeBicicletasDisponiveis=0;

    tipoUtente Utentes[MAX_UTENTE];
    int quantidadeUtentes=0;

    tipoEmprestimo *Emprestimos;
    int quantidadeEmprestimos=0;

    tipoEmprestimo *ListaDeEspera;
    int quantidadeListaDeEspera=0;

    int distanciaPercorridaTotal=0;
    int quantidadeAvarias=0;

    char opcao, opcaoCriterio='O', opcaoAdmin, opcaoUser, opcaoBicicletas, opcaoUtentes, opcaoEstatistica, opcaoSN;
    int i, x, varInteiro, pos, codigoUtente, posEmprestimo, posUtente, bicicletasAvariadas;

    tipoPontoTemporal terminoEmprestimo;
    tipoEmprestimo emprestimo;

    int autoSaveInt=1;
    Emprestimos=NULL;
    ListaDeEspera=NULL;

    autoLoad("autosave.dat", &autoSaveInt, &opcaoCriterio, &Emprestimos, &quantidadeEmprestimos, &ListaDeEspera, &quantidadeListaDeEspera, Utentes, &quantidadeUtentes, Bicicletas, &quantidadeBicicletas, &quantidadeAvarias, &distanciaPercorridaTotal);

    do{
        if(quantidadeBicicletas>0)
        {
            quantidadeBicicletasDisponiveis = totalBicicletasEmEstado(Bicicletas, quantidadeBicicletas, ESTADO_BICICLETA_DISPONIVEL);
        }
        if(quantidadeEmprestimos>0)
        {
            distanciaPercorridaTotal = distanciaTotalTodosEmprestimos(Emprestimos, quantidadeEmprestimos);
        }

        cabecalho(autoSaveInt, quantidadeBicicletas, quantidadeBicicletasDisponiveis, quantidadeEmprestimos, quantidadeListaDeEspera, quantidadeUtentes, distanciaPercorridaTotal, quantidadeAvarias);
        opcao = menuPrincipal();
        switch(opcao)
        {
        case 'A':
            do{ /* (opcaoAdmin!='V'); */
                bicicletasAvariadas = mostarBicicletas_LocalizacaoEstado(0, Bicicletas, quantidadeBicicletas, -1, ESTADO_BICICLETA_AVARIADA, localizacao, estado_bicicleta);
                opcaoAdmin = menuAdmin(bicicletasAvariadas);
                switch(opcaoAdmin)
                {
                case 'B': /* Bicicletas */
                    do{ /* (opcaoBicicletas!='V'); */
                        opcaoBicicletas = menuVar_Add_Upd_Rem(quantidadeBicicletas, "Bicicleta");
                        switch(opcaoBicicletas)
                        {
                        case 'I': /* inserir bicicleta */
                            inserirBicicleta(Bicicletas, &quantidadeBicicletas, localizacao);
                            break;
                        case 'A': /* alterar bicicleta */
                            alterarBicicleta(Bicicletas, quantidadeBicicletas, localizacao, estado_bicicleta);
                            break;
                        case 'R': /* remover bicicleta */
                            removeBicicleta(Bicicletas, &quantidadeBicicletas, localizacao, estado_bicicleta);
                            break;
                        case 'M': /* mostrar bicicletas */
                            if(quantidadeBicicletas>0)
                            {
                                mostraBicicletas(Bicicletas, quantidadeBicicletas, localizacao, estado_bicicleta);
                            }else{
                                printf("Ainda nao registou bicicletas\n");
                            }
                            printf("\n");
                            break;
                        case 'C': /* carregar bicicletas */
                            carregarBicicletasBin("bicicletas.dat", Bicicletas, &quantidadeBicicletas);
                            break;
                        case 'G': /* guardar bicicletas */
                            if(quantidadeBicicletas>0)
                            {
                                guardarBicicletasBin("bicicletas.dat", Bicicletas, quantidadeBicicletas);
                            }else{
                                printf("Ainda nao registou bicicletas\n\n");
                            }
                            break;
                        } /* switch opcaoBicicletas */
                    }while(opcaoBicicletas!='V');
                    break;
                case 'R': /* Reparar Bicicleta */
                    repararBicicleta(Bicicletas, quantidadeBicicletas, localizacao, estado_bicicleta);
                    break;
                case 'U': /* Utentes */
                    do{ /* (opcaoUtentes!='V'); */
                        opcaoUtentes = menuVar_Add_Upd_Rem(quantidadeUtentes, "Utente");
                        switch(opcaoUtentes)
                        {
                            case 'I': /* inserir utente */
                                inserirUtente(Utentes, &quantidadeUtentes, utente_tipo);
                                break;
                            case 'A': /* alterar utente */
                                alterarUtente(Utentes, quantidadeUtentes, utente_tipo);
                                break;
                            case 'R': /* remover utente */
                                removeUtenteReduzido(Utentes, &quantidadeUtentes, utente_tipo);
                                break;
                            case 'M': /* mostrar utentes */
                                if(quantidadeUtentes>0)
                                {
                                    mostraUtentes(Utentes, quantidadeUtentes, utente_tipo);
                                }else{
                                    printf("Ainda nao registou utentes");
                                }
                                printf("\n\n");
                                break;
                            case 'C': /* carregar utentes */
                                carregarUtentesBin("utentes.dat", Utentes, &quantidadeUtentes);
                                break;
                            case 'G': /* guardar utentes */
                                if(quantidadeUtentes>0)
                                {
                                    guardarUtentesBin("utentes.dat", Utentes, quantidadeUtentes);
                                }else{
                                    printf("Ainda nao registou utentes\n\n");
                                }
                                break;
                        } /* switch opcaoUtentes */
                    }while(opcaoUtentes!='V');
                    break;
                case 'A': /* Criterio de atribuicao de bicicletas */
                    opcaoCriterio = menuCriterioDeAtribuicao();
                    if(opcaoCriterio != 'V')
                    {
                        ordenaListaDeEspera(opcaoCriterio, ListaDeEspera, quantidadeListaDeEspera);
                    }
                    break;
                case 'G': /* Guardar Emprestimos*/
                        if(quantidadeEmprestimos>0)
                        {
                            guardarEmprestimosBin("emprestimos.dat", Emprestimos, quantidadeEmprestimos);
                            guardarEmprestimosBin("listadeespera.dat", ListaDeEspera, quantidadeListaDeEspera);
                        }else{
                            printf("Ainda nao registou emprestimos\n");
                        }
                        break;
                case 'C': /* Carregar Emprestimos*/
                        carregarEmprestimosBin("emprestimos.dat", &Emprestimos, &quantidadeEmprestimos);
                        carregarEmprestimosBin("listadeespera.dat", &ListaDeEspera, &quantidadeListaDeEspera);
                        break;
                case 'S': /* autosave */
                    do{
                        printf("Pretende continuar a guardar/carregar automaticamente todos os dados?\n(S)im/(N)ao\n> ");
                        varInteiro = scanf("%c", &opcaoSN);
                        opcaoSN = toupper(opcaoSN);
                        clearStdIn();
                    }while(opcaoSN != 'S' && opcaoSN != 'N' && varInteiro == 0);
                    if(opcaoSN)
                    {
                        autoSaveInt=1;
                    }else{
                        autoSaveInt=0;
                    }
                    break;
                } /* switch - opcaoAdmin = menuAdmin() */
                if(quantidadeListaDeEspera>0)
                {
                    ordenaListaDeEspera(opcaoCriterio, ListaDeEspera, quantidadeListaDeEspera);
                    for(i=0;i<LOCALIZACAO_BICICLETA_COUNT;i++)
                    {
                        varInteiro = mostarBicicletas_LocalizacaoEstado(0, Bicicletas, quantidadeBicicletas, i, ESTADO_BICICLETA_DISPONIVEL, localizacao, estado_bicicleta);
                        if(i==0 && varInteiro>0)
                        {
                            terminoEmprestimo = lePontoTemporal("Bicicletas disponiveis para utentes em espera \nIntroduza a data e hora atual: ");
                        }
                        for(x=0;x<varInteiro;x++)
                        {
                            posEmprestimo = emEsperaLocal(ListaDeEspera, quantidadeListaDeEspera, i);
                            if(posEmprestimo!=-1)
                            {
                                pos = procurarBicicletaEstadoLocal(Bicicletas, quantidadeBicicletas, ESTADO_BICICLETA_DISPONIVEL, i);
                                iniciarEmEspera(0 ,terminoEmprestimo, ListaDeEspera[posEmprestimo].numeroEmprestimo, Bicicletas[pos].designacao, &Emprestimos, &quantidadeEmprestimos, &ListaDeEspera, &quantidadeListaDeEspera, Bicicletas, quantidadeBicicletas);
                            }else{
                                x=varInteiro;
                            }
                        }
                    }
                }
            }while(opcaoAdmin!='V');
            break;
        case 'U':  /* menu utilizar */
            if(quantidadeUtentes>0 && quantidadeBicicletas>0){
                mostraUtentes(Utentes, quantidadeUtentes, utente_tipo);
                do{
                    codigoUtente = leInteiro("\nIntroduza o seu codigo de utente(0 para voltar atras)\n> ",0,50);
                    posUtente = procurarUtente(Utentes, quantidadeUtentes, codigoUtente);
                }while(posUtente==-1 && codigoUtente != 0);
                if(codigoUtente!=0)
                {
                    do{ /* (opcaoUser!='V'); */
                        opcaoUser = menuUser(Utentes[posUtente], utente_tipo, quantidadeListaDeEspera);
                        switch(opcaoUser)
                        {
                        case 'N': /* novo emprestimo */
                            if(Utentes[posUtente].temEmprestimo!=1){
                                printf("\n\nNOVO EMPRESTIMO\n\n");
                                varInteiro = novoEmprestimo(Utentes[posUtente], &Emprestimos, &quantidadeEmprestimos, &ListaDeEspera, &quantidadeListaDeEspera, Bicicletas, quantidadeBicicletas, localizacao, estado_bicicleta);
                                if(varInteiro!=-1)
                                {
                                    Utentes[posUtente].temEmprestimo=1;
                                }
                            }else{
                                printf("\n\nNao pode criar um novo emprestimo pois ja tem um emprestimo a decorrer ou em espera\n\n");
                            }
                            break;
                        case 'A': /* alterar destino */
                            if(Utentes[posUtente].temEmprestimo==1)
                            {
                                varInteiro = escolherListaStrings("Para que localizacao pretende ir?", localizacao, 0, LOCALIZACAO_BICICLETA_COUNT);
                                posEmprestimo = procuraEmprestimoADecorrer(ListaDeEspera, quantidadeListaDeEspera, Utentes[posUtente].codigoUtente);
                                if(posEmprestimo==-1)
                                {
                                    posEmprestimo = procuraEmprestimoADecorrer(Emprestimos, quantidadeEmprestimos, Utentes[posUtente].codigoUtente);
                                    Emprestimos[posEmprestimo].codigoLocalizacaoDestino=varInteiro;
                                }else{
                                    ListaDeEspera[posEmprestimo].codigoLocalizacaoDestino=varInteiro;
                                }
                            }
                            break;
                        case 'F': /* finalizar emprestimo */
                            if(Utentes[posUtente].temEmprestimo==1)
                            {
                                /* tenta iniciar da lista de espera dentro da funcao finalizar */
                                varInteiro = finalizacaoEmprestimo(&(Utentes[posUtente]), &Emprestimos, &quantidadeEmprestimos, &ListaDeEspera, &quantidadeListaDeEspera, Bicicletas, quantidadeBicicletas, localizacao, estado_bicicleta);
                            }else{
                                printf("\n\nAinda nao iniciou qualquer emprestimo se pretende cancelar um \nemprestimo em lista de espera escolha cancelar emprestimo\n\n");
                            }
                            break;
                        case 'C': /* cancelar emprestimo */
                            printf("\n\nCANCELAR EMPRESTIMO\n\n");
                            posEmprestimo = procuraEmprestimoADecorrer(Emprestimos, quantidadeEmprestimos, codigoUtente);
                            if(Utentes[posUtente].temEmprestimo==1)
                            {
                                if(posEmprestimo!=-1)
                                {
                                    emprestimo = Emprestimos[posEmprestimo];
                                    varInteiro = emEsperaLocal(ListaDeEspera, quantidadeListaDeEspera, Emprestimos[posEmprestimo].codigoLocalizacaoOrigem);
                                    if(varInteiro!=-1) /* se encontrar em lista de espera */
                                    {
                                        terminoEmprestimo = removeEmprestimo(1, emprestimo.numeroEmprestimo, ESTADO_BICICLETA_DISPONIVEL, &Emprestimos, &quantidadeEmprestimos, Bicicletas, quantidadeBicicletas);
                                        iniciarEmEspera(0, terminoEmprestimo, varInteiro, emprestimo.designacao, &Emprestimos, &quantidadeEmprestimos, &ListaDeEspera, &quantidadeListaDeEspera, Bicicletas, quantidadeBicicletas);
                                    }else{
                                        removeEmprestimo(0, emprestimo.numeroEmprestimo, ESTADO_BICICLETA_DISPONIVEL, &Emprestimos, &quantidadeEmprestimos, Bicicletas, quantidadeBicicletas);
                                    }
                                }else{
                                    posEmprestimo = procuraEmprestimoADecorrer(ListaDeEspera, quantidadeEmprestimos, codigoUtente);
                                    removeEmprestimo(0, ListaDeEspera[posEmprestimo].numeroEmprestimo, ESTADO_BICICLETA_DISPONIVEL, &ListaDeEspera, &quantidadeListaDeEspera, Bicicletas, quantidadeBicicletas);
                                }
                                Utentes[posUtente].temEmprestimo=0;
                            }else{
                                printf("\nPara cancelar um emprestimo tem de ter um emprestimo a decorrer ou em lista de espera\n\n");
                            }
                            break;
                        case 'R': /* reportar avaria */
                            reportAvaria(&(Utentes[posUtente]), ficheiro_avarias, &quantidadeAvarias, &Emprestimos, &quantidadeEmprestimos, Bicicletas, quantidadeBicicletas, localizacao, estado_bicicleta);
                            break;
                        case 'M': /* mostrar emprestimos */
                            if(quantidadeEmprestimos>0)
                            {
                                printf("\n\nLista de emprestimos emprestimos(finalizados e a decorrer):\n\n");
                                mostrarEmprestimos(Emprestimos, quantidadeEmprestimos, localizacao);
                                printf("\n\n");
                            }else{
                                printf("Ainda nao foram efetuados emprestimos\n\n");
                            }
                            break;
                        case 'L': /* mostrar emprestimos em lista de espera */
                            if(quantidadeListaDeEspera>0)
                            {
                                printf("\n\nEmprestimos em lista de espera:\n\n");
                                mostrarEmprestimos(ListaDeEspera, quantidadeListaDeEspera, localizacao);
                            }else{
                                printf("Ainda nao existem emprestimos em espera\n\n");
                            }
                            break;
                        } /* switch - opcaoUser = menuUser */
                        ordenaListaDeEspera(opcaoCriterio, ListaDeEspera, quantidadeListaDeEspera);
                    }while(opcaoUser!='V');
                }
            }else{
                printf("\nAinda nao foram introduzidos dados os dados necessarios para utilizar o programa\n\n");
            }
            break;
            case 'E': /* estatistica */
                if(quantidadeEmprestimos>0)
                {
                    do{ /* while(opcaoEstatistica!='V'); */
                        opcaoEstatistica = menuEstatistica();
                        switch(opcaoEstatistica)
                        {
                        case 'D': /* (D)istancia media percorrida por cada bicicleta */
                            mostrarDistanciaMediaPercorridaPorBicicleta(Emprestimos, quantidadeEmprestimos, Bicicletas, quantidadeBicicletas, localizacao, estado_bicicleta);
                            break;
                        case 'Q': /* (Q)uantidade de emprestimos entre duas datas */
                            quantidadeDeEmprestimosEntreDuasDatas(Emprestimos, quantidadeEmprestimos);
                            break;
                        case 'P': /* (P)ercentagem de empr�stimos efetuados por cada tipo de utente */
                            mostrarPercentagensPorTipo(Emprestimos, quantidadeEmprestimos, Utentes, quantidadeUtentes, utente_tipo, TIPO_UTENTE_COUNT);
                            break;
                        case 'C': /* (C)ampus de origem com a maior quantidade de empr�stimos */
                            mostraCampusComMaiorQuantiadeDeEmprestimos(Emprestimos, quantidadeEmprestimos, localizacao);
                            break;
                        case 'N': /* (N)umero de utentes que utilizaram uma determinada bicicleta */
                            mostrarNumeroDeUtentesQueUtilizaramUmaBicicleta(Emprestimos, quantidadeEmprestimos, Bicicletas, quantidadeBicicletas, localizacao, estado_bicicleta);
                            break;
                        case 'M': /* (M)ostrar os utentes por ordem decrescente da quantidade de emprestimos efetuados. */
                            ordenaUtentesPorMaisEmprestimosDecres(Emprestimos, quantidadeEmprestimos, Utentes, quantidadeUtentes, utente_tipo);
                            break;
                        case 'U': /* (U)ltima bicicleta utilizada por um utente e o historico de emprestimos */
                            ultimaBicicleta_MostraEmprestimos(Emprestimos, quantidadeEmprestimos, Utentes, quantidadeUtentes, Bicicletas, quantidadeBicicletas, utente_tipo, localizacao, estado_bicicleta);
                            break;
                        }/* opcaoEstatistica - menuEstatistica */
                    }while(opcaoEstatistica!='V');
                }else{
                    printf("Ainda nao foi efetuado nenhum emprestimo\n\n");
                }
                break;
            } /* switch - opcao = menuPrincipal */
    }while(opcao != 'S');
    autoSave("autosave.dat", autoSaveInt, opcaoCriterio, Emprestimos, quantidadeEmprestimos, ListaDeEspera, quantidadeListaDeEspera, Utentes, quantidadeUtentes, Bicicletas, quantidadeBicicletas, quantidadeAvarias, distanciaPercorridaTotal);

    free(Emprestimos);
    free(ListaDeEspera);
    return 0;
}

void cabecalho(int autosave, int quantidadeBicicletas, int bicicletasDisponiveis, int quantidadeEmprestimos, int quantidadeListaDeEspera, int quantidadeUtentes, int distanciaPercorridaTotal, int quantidadeAvarias)
{
    printf("\n\n\nAutosave: ");
    if(autosave)
    {
        printf("ON");
    }else{
        printf("OFF");
    }
    printf("\tUtentes: %d/50\n", quantidadeUtentes);
    printf("Bicicletas registadas: %d/15\tBicicletas disponiveis: %d/%d\n", quantidadeBicicletas, bicicletasDisponiveis, quantidadeBicicletas);
    printf("Distancia percorrida total(metros): %d\tQuantidade de Avarias: %d\n", distanciaPercorridaTotal, quantidadeAvarias);
    printf("Emprestimos: %d\tLista de espera: %d\n\n", quantidadeEmprestimos, quantidadeListaDeEspera);
}

char menuPrincipal()
{
    char opcao;
    do{
        printf("\n---------------------------------Menu principal---------------------------------\n\n");
        printf("\t(A)dministrar\n");
        printf("\t(U)tilizar\n");
        printf("\t(E)statistica\n");
        printf("\n(S)air\n> ");
        opcao = getchar();
        clearStdIn();
        opcao = toupper(opcao);
    }while(opcao != 'A' && opcao != 'U' && opcao!='E' && opcao!='S');
	return opcao;
}

char menuAdmin(int bicicletasAvariadas)
{
	char opcao;
    do{
		printf("\n-------------------------------Menu Administrador-------------------------------\n\n");
		printf("\t(B)icicletas\n");
		if(bicicletasAvariadas>0)
        {
            printf("\t(R)eparar Bicicleta\n");
        }
        printf("\t(U)tentes\n");
        printf("\tCriterio de (A)tribuicao de bicicletas\n");
		printf("\t(G)uardar Emprestimos/Lista de espera\n");
		printf("\t(C)arregar Emprestimos/Lista de espera\n");
        printf("\tDesligar/ligar Auto(S)ave\n");
		printf("\n(V)oltar ao menu anterior\n> ");
        opcao = getchar();
        clearStdIn();
        opcao = toupper(opcao);
    }while((bicicletasAvariadas<0 && (opcao != 'V' && opcao != 'B' && opcao != 'U' && opcao != 'G' && opcao != 'C' && opcao != 'A')) || (bicicletasAvariadas>0 && (opcao != 'V' && opcao != 'B' && opcao != 'U' && opcao != 'G' && opcao != 'C' && opcao != 'R' && opcao != 'A')));
	return opcao;
}

char menuCriterioDeAtribuicao()
{
    char opcao;
    do{printf("\n----------------------Menu Criterio de atribuicao bicicleta---------------------\n\n");
        printf("\tPriorizar (D)istancia\n");
        printf("\tPriorizar (O)rdem de insersao\n");
        printf("\tPriorizar (T)ipo de utente\n");
        printf("\n(V)oltar ao menu anterior\n> ");
        opcao = getchar();
        clearStdIn();
        opcao = toupper(opcao);
    }while(opcao != 'V' && opcao != 'D' && opcao != 'O' && opcao != 'T');
    return opcao;
}

char menuUser(tipoUtente utente, char utente_tipo[][MAX_STRING_GLOBAIS], int quantidadeListaDeEspera)
{
	char opcao;
    do{
        printf("\n---------------------------------Menu Utilizador--------------------------------\n");
        printf("Utente numero: %d \tNome: %s\tTipo: %s\n\n", utente.codigoUtente, utente.nome, utente_tipo[utente.codigoTipo]);
        printf("\t(N)ovo emprestimo\n");
        if(utente.temEmprestimo==1)
        {
            printf("\t(A)lterar destino\n");
            printf("\t(F)inalizar emprestimo\n");
            printf("\t(C)ancelar emprestimo/emprestimo em espera (devolver bicicleta)\n");
        }
		printf("\t(R)eportar avaria/e terminar emprestimo\n");
		printf("\t(M)ostrar emprestimos\n");
		if(0<quantidadeListaDeEspera)
        {
            printf("\tMostrar emprestimos em (L)ista de espera\n");
        }
		printf("\n(V)oltar ao menu anterior\n> ");
        opcao = getchar();
        clearStdIn();
        opcao = toupper(opcao);
    }while((quantidadeListaDeEspera<1 && (opcao != 'V' && opcao != 'N' && opcao != 'A' && opcao != 'F' && opcao != 'C' && opcao != 'R' && opcao != 'M')) || (quantidadeListaDeEspera<1 && (opcao != 'V' && opcao != 'N' && opcao != 'A' && opcao != 'F' && opcao != 'C' && opcao != 'R' && opcao != 'M' && opcao != 'L')));
	return opcao;
}

char menuVar_Add_Upd_Rem(int quant, char var[MAX_STRING])
{
    char opcao;
    do{
        printf("-------------------------------- Menu %s --------------------------------\n\n", var);
        printf("Quantidade %ss: %d\n", var, quant);
        printf("\t(I)nserir %s\n", var);
        if(quant>0)
        {
            printf("\t(A)lterar %s\n", var);
            printf("\t(R)emover %s\n", var);
            printf("\t(M)ostrar %ss\n", var);
			printf("\t(G)uardar %ss em ficehiro\n", var);
        }
		printf("\t(C)arregar %ss de ficehiro\n", var);
		printf("\nNOTA: Ao carregar/guardar apenas o ficheiro de %ss \npode criar incongruencias em estados/estatisticas\n", var);
        printf("\n(V)oltar ao menu anterior\n> ");
        opcao = getchar();
        clearStdIn();
        opcao = toupper(opcao);
    }while((quant<1 && (opcao != 'V' && opcao != 'I' && opcao != 'C')) || (quant>0 && (opcao != 'V' && opcao != 'I' && opcao != 'A' && opcao != 'R' && opcao != 'M' && opcao != 'C' && opcao != 'G')));
    return opcao;
}

char menuEstatistica()
{
	char opcao;
    do{
        printf("\n--------------------------------Menu Estatistica--------------------------------\n");
        printf("\t(D)istancia media percorrida por cada bicicleta\n");
        printf("\t(Q)uantidade de emprestimos entre duas datas\n");
        printf("\t(P)ercentagem de emprestimos efetuados por cada tipo de utente\n");
		printf("\t(C)ampus de origem com a maior quantidade de emprestimos\n");
		printf("\t(N)umero de utentes que utilizaram uma determinada bicicleta\n");
		printf("\t(M)ostrar utentes por ordem decresc. da quant. de emprestimos efetuados\n");
		printf("\t(U)ltima bicicleta utilizada por um utente e o historico de emprestimos\n");
		printf("\n(V)oltar ao menu anterior\n> ");
        opcao = getchar();
        clearStdIn();
        opcao = toupper(opcao);
    }while(opcao != 'V' && opcao != 'D' && opcao != 'Q' && opcao != 'P' && opcao != 'C' && opcao != 'N' && opcao != 'M' && opcao != 'U');
	return opcao;
}
