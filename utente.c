#include "utente.h"

/* CRIAR UTENTE */
void inserirUtente(tipoUtente Utentes[], int *quantidadeUtentes, char utente_tipo[][MAX_STRING_GLOBAIS])
{
    int codigoUtente;
    tipoUtente novoUtente;
    if((*quantidadeUtentes)<MAX_UTENTE)
    {
        /* procura um novo codigoUtente */
        codigoUtente = novoCodigoUtente(Utentes,*quantidadeUtentes);
        novoUtente = leUtente(codigoUtente, utente_tipo);
        novoUtente.ultimoNumeroEmprestimo = -1;
        novoUtente.penultimoNumeroEmprestimo = -1;
        Utentes[*quantidadeUtentes] = novoUtente;
        (*quantidadeUtentes)++;
        printf("\nO seu codigo de utente e %d\n\n", novoUtente.codigoUtente);
    }else{
        printf("Atingiu a capacidade maxima de utentes\n\n");
    }
}

tipoUtente leUtente(int codigoUtente, char utente_tipo[][MAX_STRING_GLOBAIS])
{
    int opcao;
    tipoUtente novoUtente;
    novoUtente.codigoUtente = codigoUtente;
    novoUtente.temEmprestimo = 0;
    leString("Introduza o nome do utente: ", novoUtente.nome, MAX_STRING_NOME_UTENTE);
    opcao = escolherListaStrings("Tipo de utente:", utente_tipo, 0, TIPO_UTENTE_COUNT);
    novoUtente.codigoTipo = opcao;
    novoUtente.telefone = leInteiro("Introduza o numero de telefone do utente: ", 200000000, 999999999);
    return novoUtente;
}


int novoCodigoUtente(tipoUtente Utentes[], int quantidadeUtentes)
{
    int pos, novoCodigo=0;
    pos = procurarUtente(Utentes, quantidadeUtentes, quantidadeUtentes+1);
    if(pos!=-1)
    {
        do{
            novoCodigo++;
            pos = procurarUtente(Utentes, quantidadeUtentes, novoCodigo);
        }while(pos!=-1);
    }else{
        novoCodigo = quantidadeUtentes+1;
    }
    return novoCodigo;
}



/* ALTERAR UTENTE */
void alterarUtente(tipoUtente Utentes[], int quantidadeUtentes, char utente_tipo[][MAX_STRING_GLOBAIS])
{
    int pos, codigoUtente;
    if(quantidadeUtentes>0)
    {
        mostraUtentes(Utentes, quantidadeUtentes, utente_tipo);
        do{
            codigoUtente = leInteiro("\nIntroduza o codigo do utente pretende alterar (0 para voltar)\n> ",0,50);
            pos = procurarUtente(Utentes, quantidadeUtentes, codigoUtente);
        }while(pos==-1 && codigoUtente!=0);
        if(codigoUtente!=0)
        {
            Utentes[pos]=leUtente(codigoUtente,utente_tipo);
        }else{
            printf("\nAcao cancelada\n");
        }
    }else{
        printf("Ainda nao registou utentes\n\n");
    }
}

/* ELIMINAR UTENTE*/
void removeUtenteReduzido(tipoUtente Utentes[], int *quantidadeUtentes, char utente_tipo[][MAX_STRING_GLOBAIS])
{
    int pos, codigoUtente;
    if((*quantidadeUtentes)>0)
    {
        mostraUtentes(Utentes, *quantidadeUtentes, utente_tipo);
        do{
            codigoUtente = leInteiro("\nIntroduza o codigo do utente pretende remover (0 para voltar)\n> ",0,50);
            pos = procurarUtente(Utentes, *quantidadeUtentes, codigoUtente);
        }while(pos==-1 && codigoUtente != 0);
        if(codigoUtente!=0)
        {
            removeUtenteVetor(Utentes, quantidadeUtentes, codigoUtente);
        }else{
            printf("\nAcao cancelada\n");
        }
    }else{
        printf("Ainda nao registou utentes\n\n");
    }
}

void removeUtenteVetor(tipoUtente Utentes[], int *quantidadeUtentes, int codigoUtente)
{
    int pos;
    pos = procurarUtente(Utentes, *quantidadeUtentes, codigoUtente);
    if(pos != -1)
    {
        (*quantidadeUtentes)--;
        if((*quantidadeUtentes)>0)
        {
            Utentes[pos] = Utentes[(*quantidadeUtentes)];
        }
    }else{
        printf("\nFoi nao foi encontrada nenhum utente com o codigo introduzido\n\n");
    }
}


/* MOSTRAR UTENTE */
void mostraUtente(tipoUtente utente, char utente_tipo[][MAX_STRING_GLOBAIS])
{
    printf("Codigo utente: %d\n", utente.codigoUtente);
    printf("\t Nome: %s\n", utente.nome);
    printf("\t Tipo: %s\n", utente_tipo[utente.codigoTipo]);
    printf("\t Telefone: %d\n\n", utente.telefone);
}

void mostraUtentes(tipoUtente Utentes[], int quantidadeUtentes, char utente_tipo[][MAX_STRING_GLOBAIS])
{
    int i;
    for(i=0;i<quantidadeUtentes;i++)
    {
        mostraUtente(Utentes[i], utente_tipo);
    }
}


/* PROCURAR UTENTE */
int procurarUtente(tipoUtente Utentes[], int quantidadeUtentes, int codigoUtente)
{
    int i, varInteiro=-1;
    for(i=0;i<quantidadeUtentes;i++)
    {
        if(Utentes[i].codigoUtente == codigoUtente)
        {
            varInteiro = i;
        }
    }
    return varInteiro;
}


/* FICHEIROS */
int guardarUtentesBin(char stringFicheiroUtentes[MAX_STRING], tipoUtente Utentes[], int quantidadeUtentes)
{
    FILE *ficheiroUtentes;
    ficheiroUtentes = fopen(stringFicheiroUtentes, "wb");
    fwrite(&quantidadeUtentes, sizeof(int), 1, ficheiroUtentes);
    fwrite(Utentes, sizeof(tipoUtente), quantidadeUtentes, ficheiroUtentes);
    return fclose(ficheiroUtentes);
}

int carregarUtentesBin(char stringFicheiroUtentes[MAX_STRING], tipoUtente Utentes[], int *quantidadeUtentes)
{
    FILE *ficheiroUtentes;
    ficheiroUtentes = fopen(stringFicheiroUtentes,"rb");
    fread(quantidadeUtentes, sizeof(int), 1, ficheiroUtentes);
    fread(Utentes, sizeof(tipoUtente), *quantidadeUtentes, ficheiroUtentes);
    return fclose(ficheiroUtentes);
}


