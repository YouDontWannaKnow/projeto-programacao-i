#include "emprestimos.h"

tipoEmprestimo *alojaEmprestimos(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos)
{
    tipoEmprestimo *novoEmprestimos;
    novoEmprestimos=NULL;
    novoEmprestimos = realloc(Emprestimos, (quantidadeEmprestimos * sizeof(tipoEmprestimo)));
    if(novoEmprestimos == NULL || quantidadeEmprestimos == 0)
    {
        if(quantidadeEmprestimos!=0)
        {
            printf("Nao foi possivel alocar memoria");
        }
        novoEmprestimos = Emprestimos;
    }
    return novoEmprestimos;
}


/* CRIACAO EMPRESTIMO */
int novoEmprestimo(tipoUtente utente, tipoEmprestimo *Emprestimos[], int *quantidadeEmprestimos, tipoEmprestimo *ListaDeEspera[], int *quantidadeListaDeEspera, tipoBicicleta Bicicletas[], int quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS], char estado_bicicleta[][MAX_STRING_GLOBAIS])
{
    /*
        return -1 - emprestimo cancelado
        return  0 - emprestimo colocado em lista de espera
        return  1 - emprestimo iniciado
    */
    tipoEmprestimo emprestimo;
    int varInteiro;
    varInteiro = criarEmprestimo(&emprestimo, utente.codigoUtente, Bicicletas, quantidadeBicicletas, localizacao, estado_bicicleta);
    if(varInteiro!=-1)
    {
        emprestimo.codigoTipo=utente.codigoTipo;
        if(varInteiro==1)
        {
            *Emprestimos = alojaEmprestimos(*Emprestimos, (*quantidadeEmprestimos)+1);
            emprestimo.numeroEmprestimo = novoNumeroEmprestimo(*Emprestimos, *quantidadeEmprestimos);
            (*Emprestimos)[(*quantidadeEmprestimos)] = emprestimo;
            (*quantidadeEmprestimos)++;
        }else{
            *ListaDeEspera = alojaEmprestimos(*ListaDeEspera, (*quantidadeListaDeEspera)+1);
            emprestimo.numeroEmprestimo = novoNumeroEmprestimoEspera(*ListaDeEspera, *quantidadeListaDeEspera);
            emprestimo.inicioEmprestimo.hora.hora = -1;
            (*ListaDeEspera)[(*quantidadeListaDeEspera)] = emprestimo;
            (*quantidadeListaDeEspera)++;
        }
    }
    return varInteiro;
}

int criarEmprestimo(tipoEmprestimo *EmprestimoEmCurso, int codigoUtente, tipoBicicleta Bicicletas[], int quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS], char estado[][MAX_STRING_GLOBAIS])
{
    /*
        return -1 - emprestimo cancelado
        return  0 - emprestimo colocado em lista de espera
        return  1 - emprestimo iniciado
    */
    int controlo, pos, quantidadeBicicletasLocal, varReturn;
    char designacao[MAX_STRING_DESIGNACAO], opcaoSN;
    tipoPontoTemporal inicio;
    tipoEmprestimo novoEmprestimo;
    novoEmprestimo = introducaoDadosIndependentesEmprestimo(codigoUtente, localizacao);
    quantidadeBicicletasLocal = mostarBicicletas_LocalizacaoEstado(0, Bicicletas, quantidadeBicicletas, novoEmprestimo.codigoLocalizacaoOrigem, ESTADO_BICICLETA_DISPONIVEL, localizacao, estado);
    if(quantidadeBicicletasLocal<1)
    {
        do{
            printf("Neste momento nao existem bicicletas disponiveis\nPretende ficar em lista de espera ? (S)im/(N)ao\n> ");
            controlo = scanf("%c", &opcaoSN);
            opcaoSN = toupper(opcaoSN);
            clearStdIn();
        }while(opcaoSN != 'S' && opcaoSN != 'N' && controlo == 0);
        if(opcaoSN == 'S')
        {
            (*EmprestimoEmCurso) = novoEmprestimo;
            varReturn = 0;
        }else{
            printf("\n\nCriacao de emprestimo cancelada\n\n");
            varReturn = -1;
        }
    }else{
        do{
            mostarBicicletas_LocalizacaoEstado(1,Bicicletas, quantidadeBicicletas, novoEmprestimo.codigoLocalizacaoOrigem, ESTADO_BICICLETA_DISPONIVEL, localizacao, estado);
            pos = leDesignacao("Escolha uma das bicicletas apresentadas para o emprestimo: ", designacao, Bicicletas, quantidadeBicicletas);
            if(pos!=-1 && pos!=-2)
            {
                if(Bicicletas[pos].codigoEstado!=ESTADO_BICICLETA_DISPONIVEL || Bicicletas[pos].codigoLocalizacao!=novoEmprestimo.codigoLocalizacaoOrigem)
                {
                    printf("A bicicleta que escolheu de momento nao esta disponivel\n\n");
                    pos = -1;
                }
            }
        }while(pos==-1 && pos!=-2);
        if(pos==-2)
        {
            printf("\n\nCriacao de emprestimo cancelada\n\n");
            varReturn = -1;
        }else{
            inicio = lePontoTemporal("Introduza o momento em que levantou a bicicleta");
            (*EmprestimoEmCurso) = introducaoDadosDependentesEmprestimo(novoEmprestimo, &(Bicicletas[pos]), inicio);
            varReturn = 1;
        }
    }
    return varReturn;
}

tipoEmprestimo introducaoDadosIndependentesEmprestimo(int codigoUtente, char localizacao[][MAX_STRING_GLOBAIS])
{
    /* dados independentes de disponiblidade de bicicleta */
    tipoEmprestimo emprestimo;
    emprestimo.distanciaPercorrida = 0;
    emprestimo.finalEmprestimo.hora.hora = -1;
    emprestimo.codigoUtente = codigoUtente;
    emprestimo.codigoLocalizacaoOrigem = escolherListaStrings("Introduza a sua localizacao atual:", localizacao, 0, LOCALIZACAO_BICICLETA_COUNT);
    emprestimo.codigoLocalizacaoDestino = escolherListaStrings("Defina o local onde pretende entregar a bicicleta:", localizacao, 0, LOCALIZACAO_BICICLETA_COUNT);
    return emprestimo;
}

tipoEmprestimo introducaoDadosDependentesEmprestimo(tipoEmprestimo emprestimo, tipoBicicleta *bicicleta, tipoPontoTemporal inicioEmprestimo)
{
    /* dados dependentes de disponiblidade de bicicleta */
    emprestimo.codigoLocalizacaoOrigem = bicicleta->codigoLocalizacao;
    strcpy(emprestimo.designacao,bicicleta->designacao);
    bicicleta->codigoEstado = ESTADO_BICICLETA_EMPRESTADA;
    emprestimo.inicioEmprestimo = inicioEmprestimo;
    return emprestimo;
}

int novoNumeroEmprestimo(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos)
{
    int i, numeroEmprestimo=0;
    if(quantidadeEmprestimos!=0)
    {
        for(numeroEmprestimo=0;i!=-1;numeroEmprestimo++)
        {
            i = procuraEmprestimo(Emprestimos, quantidadeEmprestimos, numeroEmprestimo);
        }
        numeroEmprestimo--;
    }
    return numeroEmprestimo;
}

int novoNumeroEmprestimoEspera(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos)
{
    int i, numeroEmprestimo=0;
    if(quantidadeEmprestimos!=0)
    {
        for(i=0;i<quantidadeEmprestimos;i++)
        {
            if(Emprestimos[i].numeroEmprestimo>=numeroEmprestimo)
            {
                numeroEmprestimo = Emprestimos[i].numeroEmprestimo + 1;
            }
        }
    }
    return numeroEmprestimo;
}


/* FINALIZA EMPRESTIMO */
int finalizacaoEmprestimo(tipoUtente *utente, tipoEmprestimo *Emprestimos[], int *quantidadeEmprestimos, tipoEmprestimo *ListaDeEspera[], int *quantidadeListaDeEspera, tipoBicicleta Bicicletas[], int quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS], char estado[][MAX_STRING_GLOBAIS])
{
    /* apos entrega de bicicleta - finalizacao normal de emprestimo */
    /*
        return -1 - finalizacao emprestimo cancelada
        return  0 - finalizacao emprestimo concluida
        return  1 - emprestimo da lista de espera iniciado
    */
    int posEmprestimo, varInteiro, i, varReturn;
    printf("\n\nFINALIZAR EMPRESTIMO\n\n");
    posEmprestimo = procuraEmprestimoADecorrer(*Emprestimos, *quantidadeEmprestimos, utente->codigoUtente);
    if(posEmprestimo!=-1)
    {
        varInteiro = mostarBicicletas_LocalizacaoEstado(0, Bicicletas, quantidadeBicicletas, (*Emprestimos)[posEmprestimo].codigoLocalizacaoDestino, ESTADO_BICICLETA_DISPONIVEL, localizacao, estado);
        i=0;
        do{
            if(i%5==0 && i!=0 && (*Emprestimos)[posEmprestimo].distanciaPercorrida==0)
            {
                printf("\n\nTentativa(%d) caso pretenda devolver a bicicleta introduza '-1' e \nEscolha a opcao 'Cancelar emprestimo/emprestimo em espera (devolver bicicleta)' no menu de utilizador\n", i);
            }
            printf("Introduza a distancia percorrida(metros): ");
            varInteiro = scanf("%d", &((*Emprestimos)[posEmprestimo].distanciaPercorrida));
            clearStdIn();
            i++;
        }while(((*Emprestimos)[posEmprestimo].distanciaPercorrida <= 0 || varInteiro == 0) && (*Emprestimos)[posEmprestimo].distanciaPercorrida!=-1);
        if((*Emprestimos)[posEmprestimo].distanciaPercorrida!=-1)
        {
            do{
                (*Emprestimos)[posEmprestimo].finalEmprestimo = lePontoTemporal("\nIntroduza o momento em que devolveu a bicicleta");
                varInteiro = verificaPontoTemporal((*Emprestimos)[posEmprestimo].inicioEmprestimo, (*Emprestimos)[posEmprestimo].finalEmprestimo);
                if(varInteiro!=1)
                {
                    printf("A data e ou hora que introduzio nao sao validas \n(Inicio do emprestimo: %d:%d:%d - %d/%d/%d)\n\n", (*Emprestimos)[posEmprestimo].inicioEmprestimo.hora.hora, (*Emprestimos)[posEmprestimo].inicioEmprestimo.hora.minutos, (*Emprestimos)[posEmprestimo].inicioEmprestimo.hora.segundos, (*Emprestimos)[posEmprestimo].inicioEmprestimo.data.dia, (*Emprestimos)[posEmprestimo].inicioEmprestimo.data.mes, (*Emprestimos)[posEmprestimo].inicioEmprestimo.data.ano);
                }
            }while(varInteiro!=1);
            varInteiro = procuraBicicleta(Bicicletas, quantidadeBicicletas, (*Emprestimos)[posEmprestimo].designacao);
            Bicicletas[varInteiro].codigoEstado = ESTADO_BICICLETA_DISPONIVEL;
            Bicicletas[varInteiro].codigoLocalizacao = (*Emprestimos)[posEmprestimo].codigoLocalizacaoDestino;
            utente->temEmprestimo=0;
            /* verifica lista de espera | varInteiro = numero de emprestimo em espera se existir */
            varInteiro = emEsperaLocal(*ListaDeEspera,*quantidadeListaDeEspera, (*Emprestimos)[posEmprestimo].codigoLocalizacaoDestino);
            if(varInteiro!=-1)
            {
                /* muda 1 de lista de espera para lista emprestimos */
                iniciarEmEspera(0, (*Emprestimos)[posEmprestimo].finalEmprestimo, varInteiro, (*Emprestimos)[posEmprestimo].designacao, Emprestimos, quantidadeEmprestimos, ListaDeEspera, quantidadeListaDeEspera, Bicicletas, quantidadeBicicletas);
                varInteiro=1;
            }
        }else{
            printf("\nFinalizacao de emprestimo cancelada\n");
            (*Emprestimos)[posEmprestimo].distanciaPercorrida=0;
            varInteiro=-1;
        }
    }else{
        printf("O utente nao tem emprestimos a decorrer");
        varReturn=-1;
    }
    return varReturn;
}


/* ELIMINA EMPRESTIMO */
tipoPontoTemporal removeEmprestimo(int lerPontoTemp, int numeroEmprestimo, int estado_bicicleta, tipoEmprestimo *Emprestimos[], int *quantidadeEmprestimos, tipoBicicleta Bicicletas[], int quantidadeBicicletas)
{
    /* remove emprestimo, todos os seguintes regridem uma posicao no vetor e realoja o vetor em memoria */
    int i, posBicicleta, posEmprestimo;
    tipoPontoTemporal ptmp;
    posEmprestimo = procuraEmprestimo(*Emprestimos, *quantidadeEmprestimos, numeroEmprestimo);
    if(posEmprestimo!=-1)
    {
        posBicicleta = procuraBicicleta(Bicicletas, quantidadeBicicletas, (*Emprestimos)[posEmprestimo].designacao);
        if(posBicicleta!=-1)
        {
            Bicicletas[posBicicleta].codigoEstado = estado_bicicleta;
            Bicicletas[posBicicleta].codigoLocalizacao = (*Emprestimos)[posEmprestimo].codigoLocalizacaoOrigem;
        }
        for(i=posEmprestimo;i<(*quantidadeEmprestimos);i++)
        {
            (*Emprestimos)[i]=(*Emprestimos)[i+1];
        }
        (*quantidadeEmprestimos)--;
        *Emprestimos = alojaEmprestimos(*Emprestimos, *quantidadeEmprestimos);
        if(lerPontoTemp==1)
        {
            ptmp = lePontoTemporal("Introduza a data e hora:");
        }
    }
    else
    {
        printf("\n\n\nO numero de emprestimo introduzido nao existe\n\n\n");
    }
    return ptmp;
}


/* PROCURA EMPRESTIMOS */
int procuraEmprestimo(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, int numeroEmprestimo)
{
    int i, varReturn=-1;
    for(i=0;i<quantidadeEmprestimos;i++)
    {
        if(Emprestimos[i].numeroEmprestimo==numeroEmprestimo)
        {
            varReturn=i;
            i=quantidadeEmprestimos;
        }
    }
    return varReturn;
}

int procuraEmprestimoADecorrer(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, int codigoUtente)
{
    int i, varReturn=-1;
    for(i=0;i<quantidadeEmprestimos;i++)
    {
        if(Emprestimos[i].finalEmprestimo.hora.hora==-1 && Emprestimos[i].codigoUtente==codigoUtente)
        {
            varReturn=i;
            i=quantidadeEmprestimos;
        }
    }
    return varReturn;
}

int procurarUltimoEmprestimoUtente(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, int codigoUtente)
{
    int i, varInteiro=0, varReturn=-1;
    for(i=quantidadeEmprestimos-1;i>-1;i--)
    {
        if(Emprestimos[i].codigoUtente==codigoUtente && varInteiro<Emprestimos[i].numeroEmprestimo)
        {
            varReturn=i;
            varInteiro=Emprestimos[i].numeroEmprestimo;
        }
    }
    return varReturn;
}



/* CALCULOS */
int distanciaTotalTodosEmprestimos(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos)
{
    int distanciaTotal=0,i;
    for(i=0;i<quantidadeEmprestimos;i++)
    {
        if(Emprestimos[i].distanciaPercorrida>0)
        {
            distanciaTotal += Emprestimos[i].distanciaPercorrida;
        }
    }
    return distanciaTotal;
}

int distanciaTotalUtente(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, int codigoUtente)
{
    int distanciaTotal=0,i;
    for(i=0;i<quantidadeEmprestimos;i++)
    {
        if(Emprestimos[i].distanciaPercorrida>0 && Emprestimos[i].codigoUtente==codigoUtente)distanciaTotal += Emprestimos[i].distanciaPercorrida;
    }
    return distanciaTotal;
}

int distanciaTotalBicicleta(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, char designacaoBicicleta[MAX_STRING_DESIGNACAO])
{
    int distanciaTotal=0,i;
    for(i=0;i<quantidadeEmprestimos;i++)
    {
        if(Emprestimos[i].distanciaPercorrida>0 && strcasecmp(designacaoBicicleta, Emprestimos[i].designacao))distanciaTotal += Emprestimos[i].distanciaPercorrida;
    }
    return distanciaTotal;
}



/* MOSTRAR */
void mostrarEmprestimo(tipoEmprestimo emprestimo, char localizacao[][MAX_STRING_GLOBAIS])
{
    printf("Emprestimo - %d\n", emprestimo.numeroEmprestimo);
    printf("\tCodigo Utente: %d\n", emprestimo.codigoUtente);
    printf("\tOrigem: %s\n", localizacao[emprestimo.codigoLocalizacaoOrigem]);
    printf("\tDestino: %s\n", localizacao[emprestimo.codigoLocalizacaoDestino]);
    if(emprestimo.finalEmprestimo.hora.hora != -1)
    {
        printf("\tBicicleta: %s\n", emprestimo.designacao);
        printf("\tInicio: %d/%d/%d - %d:%d:%d\n", emprestimo.inicioEmprestimo.data.dia, emprestimo.inicioEmprestimo.data.mes, emprestimo.inicioEmprestimo.data.ano, emprestimo.inicioEmprestimo.hora.hora, emprestimo.inicioEmprestimo.hora.minutos, emprestimo.inicioEmprestimo.hora.segundos);
        printf("\tFinal: %d/%d/%d - %d:%d:%d\n", emprestimo.finalEmprestimo.data.dia, emprestimo.finalEmprestimo.data.mes, emprestimo.finalEmprestimo.data.ano, emprestimo.finalEmprestimo.hora.hora, emprestimo.finalEmprestimo.hora.minutos, emprestimo.finalEmprestimo.hora.segundos);
        printf("\tDistancia percorrida(metros): %d\n\n", emprestimo.distanciaPercorrida);
    }
    else
    {
        if(emprestimo.inicioEmprestimo.hora.hora!=-1)
        {
            printf("\tBicicleta: %s\n", emprestimo.designacao);
            printf("\tInicio: %d/%d/%d - %d:%d:%d\n", emprestimo.inicioEmprestimo.data.dia, emprestimo.inicioEmprestimo.data.mes, emprestimo.inicioEmprestimo.data.ano, emprestimo.inicioEmprestimo.hora.hora, emprestimo.inicioEmprestimo.hora.minutos, emprestimo.inicioEmprestimo.hora.segundos);
        }else{
            printf("\tBicicleta: EM ESPERA\n");
            printf("\tInicio: --/--/-- - --:--:--\n");
        }
        printf("\tFinal: --/--/-- - --:--:--\n");
        printf("\tDistancia percorrida(metros): ------\n\n");
    }
}

void mostrarEmprestimos(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, char localizacao[][MAX_STRING_GLOBAIS])
{
    int i;
    for(i=0;i<quantidadeEmprestimos;i++)
    {
        mostrarEmprestimo(Emprestimos[i], localizacao);
    }
}

int mostrarEmprestimosUtente(int mostrar, tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, int codigoUtente, char localizacao[][MAX_STRING_GLOBAIS])
{
    int i, count=0;
    for(i=0;i<quantidadeEmprestimos;i++)
    {
        if(Emprestimos[i].codigoUtente==codigoUtente)
        {
            if(mostrar==1)
            {
                mostrarEmprestimo(Emprestimos[i], localizacao);
                count++;
            }else{
                count++;
            }
        }
    }
    return count;
}

int mostrarEmprestimosBicicleta(int mostrar, tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, char designacao[MAX_STRING_DESIGNACAO], char localizacao[][MAX_STRING_GLOBAIS])
{
    int i, count=0;
    for(i=0;i<quantidadeEmprestimos;i++)
    {
        if(strcmp(Emprestimos[i].designacao,designacao))
        {
            if(mostrar==1)
            {
                mostrarEmprestimo(Emprestimos[i], localizacao);
                count++;
            }else{
                count++;
            }
        }
    }
    return count;
}



/* FICHEIROS */
int guardarEmprestimosBin(char stringFicheiroEmprestimos[MAX_STRING], tipoEmprestimo Emprestimos[], int quantidadeEmprestimos)
{
    FILE *ficheiroEmprestimos;
    ficheiroEmprestimos = fopen(stringFicheiroEmprestimos, "wb");
    fwrite(&quantidadeEmprestimos, sizeof(int), 1, ficheiroEmprestimos);
    fwrite(Emprestimos, sizeof(tipoEmprestimo), quantidadeEmprestimos, ficheiroEmprestimos);
    return fclose(ficheiroEmprestimos);
}

int carregarEmprestimosBin(char stringFicheiroEmprestimos[MAX_STRING], tipoEmprestimo *Emprestimos[], int *quantidadeEmprestimos)
{
    FILE *ficheiroEmprestimos;
    ficheiroEmprestimos = fopen(stringFicheiroEmprestimos,"rb");
    fread(quantidadeEmprestimos, sizeof(int), 1, ficheiroEmprestimos);
    *Emprestimos = alojaEmprestimos(*Emprestimos, *quantidadeEmprestimos);
    fread(*Emprestimos, sizeof(tipoEmprestimo), *quantidadeEmprestimos, ficheiroEmprestimos);
    return fclose(ficheiroEmprestimos);
}



/* LISTA DE ESPERA */
int emEsperaLocal(tipoEmprestimo ListaDeEspera[], int quantidadeListaDeEspera, int codigoLocal)
{
    int i, varReturn=-1;
    for(i=quantidadeListaDeEspera-1;i>-1;i--)
    {
        if(ListaDeEspera[i].codigoLocalizacaoOrigem==codigoLocal)
        {
            varReturn=ListaDeEspera[i].numeroEmprestimo;
            i=-1;
        }
    }
    return varReturn;
}

/* INICIAR A PARTIR DA LISTA DE ESPERA */
void iniciarEmEspera(int lerPontoTemp, tipoPontoTemporal inicio, int numeroEmprestimoEmEspera, char designacao[MAX_STRING_DESIGNACAO], tipoEmprestimo *Emprestimos[], int *quantidadeEmprestimos, tipoEmprestimo *ListaDeEspera[], int *quantidadeListaDeEspera, tipoBicicleta Bicicletas[], int quantidadeBicicletas)
{
    int posListaEspera;
    posListaEspera = procuraEmprestimo(*ListaDeEspera, *quantidadeListaDeEspera, numeroEmprestimoEmEspera);
    (*ListaDeEspera)[posListaEspera].numeroEmprestimo = novoNumeroEmprestimo(*Emprestimos, *quantidadeEmprestimos);
    if(lerPontoTemp==1)
    {
        inicio = lePontoTemporal("Introduza a data e hora:");
    }
    (*ListaDeEspera)[posListaEspera].inicioEmprestimo=inicio;
    strcpy((*ListaDeEspera)[posListaEspera].designacao,designacao);
    *Emprestimos = alojaEmprestimos(*Emprestimos, (*quantidadeEmprestimos)+1);
    (*Emprestimos)[*quantidadeEmprestimos]=(*ListaDeEspera)[posListaEspera];
    (*quantidadeEmprestimos)++;
    removeEmprestimo(0, (*ListaDeEspera)[posListaEspera].numeroEmprestimo, ESTADO_BICICLETA_EMPRESTADA, ListaDeEspera, quantidadeListaDeEspera, Bicicletas, quantidadeBicicletas);
}

void ordenaListaDeEspera(char tipoOrdem, tipoEmprestimo ListaDeEspera[], int quantidadeListaDeEspera)
{
    int i, x, ordem[LOCALIZACAO_BICICLETA_COUNT][LOCALIZACAO_BICICLETA_COUNT] =
    {
        {
            { 0 }, /* 0 - 0 */
            { 1 }, /* 0 - 1 */
            { 2 }, /* 0 - 2 */
            { 3 }  /* 0 - 3 */
        },
        {
            { 1 }, /* 1 - 0 */
            { 0 }, /* 1 - 1 */
            { 2 }, /* 1 - 2 */
            { 3 }  /* 1 - 3 */
        },
        {
            { 1 }, /* 2 - 0 */
            { 2 }, /* 2 - 1 */
            { 0 }, /* 2 - 2 */
            { 3 }  /* 2 - 3 */
        },
        {
            { 2 }, /* 3 - 0 */
            { 1 }, /* 3 - 1 ovelha negra campus 5 fica mais proximo de campos 1 do que das residencias */
            { 3 }, /* 3 - 2 */
            { 0 }  /* 3 - 3 */
        }
    };
    tipoEmprestimo emprestimo;
    switch(tipoOrdem)
    {
    case 'D': /* distancia */
        for(x=0;x<quantidadeListaDeEspera;x++)
        {
            for(i=0;i<quantidadeListaDeEspera-1;i++)
            {
                if(ordem[ListaDeEspera[i].codigoLocalizacaoOrigem][ListaDeEspera[i].codigoLocalizacaoDestino]>ordem[ListaDeEspera[i+1].codigoLocalizacaoOrigem][ListaDeEspera[i+1].codigoLocalizacaoDestino])
                {
                    emprestimo = ListaDeEspera[i];
                    ListaDeEspera[i] = ListaDeEspera[i+1];
                    ListaDeEspera[i+1] = emprestimo;
                }
            }
        }
        break;
    case 'O': /* ordem de insersao */
        for(x=0;x<quantidadeListaDeEspera;x++)
        {
            for(i=0;i<quantidadeListaDeEspera-1;i++)
            {
                if(ListaDeEspera[i].numeroEmprestimo > ListaDeEspera[i+1].numeroEmprestimo)
                {
                    emprestimo = ListaDeEspera[i];
                    ListaDeEspera[i] = ListaDeEspera[i+1];
                    ListaDeEspera[i+1] = emprestimo;
                }
            }
        }
        break;
    case 'T': /* tipo de utente */
        for(i=0;i<quantidadeListaDeEspera-1;i++)
        {
            if(ListaDeEspera[i].codigoTipo > ListaDeEspera[i+1].codigoTipo)
                {
                    emprestimo = ListaDeEspera[i];
                    ListaDeEspera[i] = ListaDeEspera[i+1];
                    ListaDeEspera[i+1] = emprestimo;
                }
        }
        break;
    }
}

