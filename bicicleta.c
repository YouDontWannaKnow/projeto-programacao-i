#include "bicicleta.h"

int leDesignacao(char mensagem[MAX_STRING], char designacaoBicicleta[MAX_STRING_DESIGNACAO], tipoBicicleta Bicicletas[], int quantidadeBicicletas)
{
    /*
    return -2  - cancelar
    return -1  - nao existe
    return -1<x - existe em posicao x
    */

    char sair[5] = { 'S', 'A', 'I', 'R', '\0'};
    char designacaoBicicletaCheck[MAX_STRING_DESIGNACAO];
    int varReturn;
    printf("%s\n(para cancelar escreva 'sair')\n", mensagem);
    leString("Introduza a designacao da bicicleta: ", designacaoBicicleta, MAX_STRING_DESIGNACAO);
    strcpy(designacaoBicicletaCheck, designacaoBicicleta);
    strupr(designacaoBicicletaCheck);
    if(strcmp(designacaoBicicletaCheck,sair)==0)
    {
        varReturn = -2;
    }else{
        varReturn = procuraBicicleta(Bicicletas, quantidadeBicicletas, designacaoBicicleta);
    }
    return varReturn;
}

/* CRIAR BICICLETA */
void inserirBicicleta(tipoBicicleta Bicicletas[], int *quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS])
{
    tipoBicicleta novaBicicleta;
    char designacaoBicicleta[MAX_STRING_DESIGNACAO];
    int pos;
    if((*quantidadeBicicletas)<MAX_BICICLETA)
    {
        printf("\n\n");
        do{
            pos = leDesignacao("Inserir bicicleta (cada bicicleta deve ter uma designacao unica): ", designacaoBicicleta, Bicicletas, *quantidadeBicicletas);
        }while(pos!=-1 && pos!=-2);
        if(pos==-1)
        {
            novaBicicleta = leBicicleta(1, designacaoBicicleta, localizacao);
            Bicicletas[*quantidadeBicicletas] = novaBicicleta;
            (*quantidadeBicicletas)++;
        }else{
            printf("\nAcao cancelada\n");
        }
    }else{
        printf("\nAtingiu o limite de bicicletas para adicionar outra bicicleta deve remover uma das existentes\n\n");
    }
}

tipoBicicleta leBicicleta(int inserir, char designacao[MAX_STRING_DESIGNACAO], char localizacao[][MAX_STRING_GLOBAIS])
{
    tipoBicicleta bicicleta;
    int opcaoInt;
    strcpy(bicicleta.designacao, designacao);
    leString("\nIntroduza o modelo da bicicleta: ", bicicleta.modelo, MAX_STRING_MODELO);
    if(inserir==1)
    {
        bicicleta.quantidadeAvarias=0;
        bicicleta.distanciaMediaPercorrida=0;
        bicicleta.codigoEstado = ESTADO_BICICLETA_DISPONIVEL;
    }else{
        printf("\nPara colocar a bicicleta como avariada deve proceder ao registo da avaria na opcao\nMenu principal -> 'Utilizar' -> 'Reportar avaria'\n");
    }
    opcaoInt = escolherListaStrings("\tLocalizacao:", localizacao, 0, LOCALIZACAO_BICICLETA_COUNT);
    bicicleta.codigoLocalizacao = opcaoInt;
    return bicicleta;
}

/* ALTERAR BICICLETA */
void alterarBicicleta(tipoBicicleta Bicicletas[], int quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS], char estado[][MAX_STRING_GLOBAIS])
{
    char designacaoBicicleta[MAX_STRING_DESIGNACAO];
    int pos;
    if(quantidadeBicicletas>0)
    {
        mostraBicicletas(Bicicletas, quantidadeBicicletas, localizacao, estado);
        do{
            pos = leDesignacao("\nIntroduza a designacao da bicicleta que pretende alterar: ", designacaoBicicleta, Bicicletas, quantidadeBicicletas);
        }while(pos==-1 && pos!=-2);
        if(pos!=-2)
        {
            Bicicletas[pos] = leBicicleta(0, designacaoBicicleta, localizacao);
        }else{
            printf("\nAcao cancelada\n");
        }
    }else{
        printf("\nAinda nao registou bicicletas\n\n");
    }
}

/* ELIMINAR BICICLETA */
void removeBicicleta(tipoBicicleta Bicicletas[], int *quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS], char estado[][MAX_STRING_GLOBAIS])
{
    char designacao[MAX_STRING_DESIGNACAO];
    int pos;
    if((*quantidadeBicicletas)>0)
    {
        mostraBicicletas(Bicicletas, *quantidadeBicicletas, localizacao, estado);
        do{
            pos = leDesignacao("\nIntroduza a designacao da bicicleta que pretende remover: ", designacao, Bicicletas, *quantidadeBicicletas);
        }while(pos==-1 && pos!=-2);
        if(pos!=-2)
        {
            removeBicicletaVetor(Bicicletas, quantidadeBicicletas, designacao);
        }else{
            printf("\nAcao cancelada\n");
        }
    }else{
        printf("\nAinda nao registou bicicletas\n\n");
    }
}

void removeBicicletaVetor(tipoBicicleta Bicicletas[], int *quantidadeBicicletas, char designacao[MAX_STRING_DESIGNACAO])
{
    int pos;
    pos = procuraBicicleta(Bicicletas, (*quantidadeBicicletas), designacao);
    if(pos != -1)
    {
        (*quantidadeBicicletas)--;
        if((*quantidadeBicicletas)>0) Bicicletas[pos] = Bicicletas[(*quantidadeBicicletas)];
    }else{
        printf("\nFoi nao foi encontrada nenhuma bicicleta com a designacao introduzida\n");
    }
}


/* MOSTRAR */
void mostraBicicleta(tipoBicicleta bicicleta, char localizacao[][MAX_STRING_GLOBAIS], char estado[][MAX_STRING_GLOBAIS])
{
    printf("\nDesignacao: %s\n", bicicleta.designacao);
    printf("\tModelo: %s\n", bicicleta.modelo);
    printf("\tEstado: %s\n", estado[bicicleta.codigoEstado]);
    printf("\tLocalizacao: %s\n", localizacao[bicicleta.codigoLocalizacao]);
    /*if(menu=='A')
    {
        printf("Quant. avarias: %d", bicicleta.quantidadeAvarias);
    }*/
    printf("\n");
}

void mostraBicicletas(tipoBicicleta Bicicletas[], int quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS], char estado[][MAX_STRING_GLOBAIS])
{
    int i;
    for(i=0;i<quantidadeBicicletas;i++)
    {
        mostraBicicleta(Bicicletas[i], localizacao, estado);
    }
}

int mostarBicicletas_LocalizacaoEstado(int mostrar, tipoBicicleta Bicicletas[], int quantidadeBicicletas, int codigoLocalizacao, int codigoEstado, char localizacao[][MAX_STRING_GLOBAIS], char estado[][MAX_STRING_GLOBAIS])
{
    int i, quant=0;
    for(i=0;i<quantidadeBicicletas;i++)
    {
        if((Bicicletas[i].codigoLocalizacao == codigoLocalizacao && Bicicletas[i].codigoEstado == codigoEstado) || (codigoLocalizacao == -1 && Bicicletas[i].codigoEstado == codigoEstado))
        {
            if(mostrar)
            {
                mostraBicicleta(Bicicletas[i], localizacao, estado);
            }
            quant++;
        }
    }
    return quant;
}


/* PROCURAR */
int procuraBicicleta(tipoBicicleta Bicicletas[], int quantidadeBicicletas, char designacao[MAX_STRING_DESIGNACAO])
{
    int i, varReturn=-1;
    for(i=0;i<quantidadeBicicletas;i++)
    {
        if(strcmp(designacao, Bicicletas[i].designacao)==0)
        {
            varReturn = i;
        }
    }
    return varReturn;
}

int procurarBicicletaEstadoLocal(tipoBicicleta Bicicletas[], int quantidadeBicicletas, int estado, int localizacao)
{
    int i, varReturn;
    for(i=0;i<quantidadeBicicletas;i++)
    {
        if(Bicicletas[i].codigoEstado==estado && Bicicletas[i].codigoLocalizacao==localizacao)
        {
            varReturn = i;
        }
    }
    return varReturn;
}

/* CALCULOS */
int totalBicicletasEmEstado(tipoBicicleta Bicicletas[], int quantidadeBicicletas, int Estado)
{
    int i, total=0;
    for(i=0;i<quantidadeBicicletas;i++)
    {
        if(Bicicletas[i].codigoEstado==Estado)
        {
            total++;
        }
    }
    return total;
}


/* FICHEIROS */
int guardarBicicletasBin(char stringFicheiroBicicletas[MAX_STRING], tipoBicicleta Bicicletas[], int quantidadeBicicletas)
{
    FILE *ficheiroBicicletas;
    ficheiroBicicletas = fopen(stringFicheiroBicicletas, "wb");
    fwrite(&quantidadeBicicletas, sizeof(int), 1, ficheiroBicicletas);
    fwrite(Bicicletas, sizeof(tipoBicicleta), quantidadeBicicletas, ficheiroBicicletas);
    return fclose(ficheiroBicicletas);
}

int carregarBicicletasBin(char stringFicheiroBicicletas[MAX_STRING], tipoBicicleta Bicicletas[], int *quantidadeBicicletas)
{
    FILE *ficheiroBicicletas;
    ficheiroBicicletas = fopen(stringFicheiroBicicletas,"rb");
    fread(quantidadeBicicletas, sizeof(int), 1, ficheiroBicicletas);
    fread(Bicicletas, sizeof(tipoBicicleta), (*quantidadeBicicletas), ficheiroBicicletas);
    return fclose(ficheiroBicicletas);
}


