#ifndef BICICLETA_H_INCLUDED
#define BICICLETA_H_INCLUDED
#include "camada1.h"

int leDesignacao(char mensagem[MAX_STRING], char designacaoBicicleta[MAX_STRING_DESIGNACAO], tipoBicicleta Bicicletas[], int quantidadeBicicletas);

/* CRIAR BICICLETA */
void inserirBicicleta(tipoBicicleta Bicicletas[], int *quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS]);
tipoBicicleta leBicicleta(int inserir, char designacao[MAX_STRING_DESIGNACAO], char localizacao[][MAX_STRING_GLOBAIS]);

/* ALTERAR BICICLETA */
void alterarBicicleta(tipoBicicleta Bicicletas[], int quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS], char estado[][MAX_STRING_GLOBAIS]);

/* ELIMINAR BICICLETA */
void removeBicicleta(tipoBicicleta Bicicletas[], int *quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS], char estado[][MAX_STRING_GLOBAIS]);
void removeBicicletaVetor(tipoBicicleta Bicicletas[], int *quantidadeBicicletas, char designacao[MAX_STRING_DESIGNACAO]);

/* MOSTRAR */
void mostraBicicleta(tipoBicicleta bicicleta, char localizacao[][MAX_STRING_GLOBAIS], char estado[][MAX_STRING_GLOBAIS]);
void mostraBicicletas(tipoBicicleta Bicicletas[], int quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS], char estado[][MAX_STRING_GLOBAIS]);
int mostarBicicletas_LocalizacaoEstado(int mostrar, tipoBicicleta Bicicletas[], int quantidadeBicicletas, int codigoLocalizacao, int codigoEstado, char localizacao[][MAX_STRING_GLOBAIS], char estado[][MAX_STRING_GLOBAIS]);

/* PROCURAR */
int procuraBicicleta(tipoBicicleta Bicicletas[], int quantidadeBicicletas, char designacao[MAX_STRING_DESIGNACAO]);
int procurarBicicletaEstadoLocal(tipoBicicleta Bicicletas[], int quantidadeBicicletas, int estado, int localizacao);

/* CALCULOS */
int totalBicicletasEmEstado(tipoBicicleta Bicicletas[], int quantidadeBicicletas, int Estado);

/* FICHEIROS */
int guardarBicicletasBin(char stringFicheiroBicicletas[MAX_STRING], tipoBicicleta Bicicletas[], int quantidadeBicicletas);
int carregarBicicletasBin(char stringFicheiroBicicletas[MAX_STRING], tipoBicicleta Bicicletas[], int *quantidadeBicicletas);

#endif /* BICICLETA_H_INCLUDED */
