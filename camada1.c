#include "camada1.h"

void autoSave(char ficheiroAutoSave[MAX_STRING], int guardar, char opcaoCriterio, tipoEmprestimo *Emprestimos, int quantidadeEmprestimos, tipoEmprestimo *ListaDeEspera, int quantidadeListaDeEspera, tipoUtente *Utentes, int quantidadeUtentes, tipoBicicleta *Bicicletas, int quantidadeBicicletas, int quantidadeAvarias, int distanciaPercorridaTotal)
{
    FILE *ficheiro;
    ficheiro = fopen(ficheiroAutoSave, "wb");

    fwrite(&guardar, sizeof(int), 1, ficheiro);

    if(guardar==1)
    {
        fwrite(&opcaoCriterio, sizeof(char), 1, ficheiro);

        fwrite(&quantidadeEmprestimos, sizeof(int), 1, ficheiro);
        fwrite(Emprestimos, sizeof(tipoEmprestimo), quantidadeEmprestimos, ficheiro);

        fwrite(&quantidadeListaDeEspera, sizeof(int), 1, ficheiro);
        fwrite(ListaDeEspera, sizeof(tipoEmprestimo), quantidadeListaDeEspera, ficheiro);

        fwrite(&quantidadeUtentes, sizeof(int), 1, ficheiro);
        fwrite(Utentes, sizeof(tipoUtente), quantidadeUtentes, ficheiro);

        fwrite(&quantidadeBicicletas, sizeof(int), 1, ficheiro);
        fwrite(Bicicletas, sizeof(tipoBicicleta), quantidadeBicicletas, ficheiro);

        fwrite(&quantidadeAvarias, sizeof(int), 1, ficheiro);
        fwrite(&distanciaPercorridaTotal, sizeof(int), 1, ficheiro);
    }
    fclose(ficheiro);
}

void autoLoad(char ficheiroAutoSave[MAX_STRING], int *carregar, char *opcaoCriterio, tipoEmprestimo **Emprestimos, int *quantidadeEmprestimos, tipoEmprestimo **ListaDeEspera, int *quantidadeListaDeEspera, tipoUtente *Utentes, int *quantidadeUtentes, tipoBicicleta *Bicicletas, int *quantidadeBicicletas, int *quantidadeAvarias, int *distanciaPercorridaTotal)
{
    FILE *ficheiro;
    ficheiro = fopen(ficheiroAutoSave, "rb");

    fread(carregar, sizeof(int), 1, ficheiro);

    if((*carregar)==1)
    {
        fread(opcaoCriterio, sizeof(char), 1, ficheiro);

        fread(quantidadeEmprestimos, sizeof(int), 1, ficheiro);
        *Emprestimos = alojaEmprestimos(*Emprestimos, *quantidadeEmprestimos);
        fread(*Emprestimos, sizeof(tipoEmprestimo), (*quantidadeEmprestimos), ficheiro);

        fread(quantidadeListaDeEspera, sizeof(int), 1, ficheiro);
        *ListaDeEspera = alojaEmprestimos(*ListaDeEspera, *quantidadeListaDeEspera);
        fread(*ListaDeEspera, sizeof(tipoEmprestimo), (*quantidadeListaDeEspera), ficheiro);

        fread(quantidadeUtentes, sizeof(int), 1, ficheiro);
        fread(Utentes, sizeof(tipoUtente), (*quantidadeUtentes), ficheiro);

        fread(quantidadeBicicletas, sizeof(int), 1, ficheiro);
        fread(Bicicletas, sizeof(tipoBicicleta), (*quantidadeBicicletas), ficheiro);

        fread(quantidadeAvarias, sizeof(int), 1, ficheiro);
        fread(distanciaPercorridaTotal, sizeof(int), 1, ficheiro);
    }

    fclose(ficheiro);
}

int escolherListaStrings(char mensagem[MAX_STRING], char var[][MAX_STRING_GLOBAIS], int inicioLista, int finalLista)
{
    int i;
    printf("%s\n", mensagem);
    for(i=inicioLista;i<finalLista;i++)
    {
        printf("\t\t%i - %s\n", i, var[i]);
    }
    i = leInteiro("",inicioLista,finalLista-1);
    return i;
}

void clearStdIn()
{
    char lixo;
    do
    {
        lixo = getchar();
    }
    while(lixo != '\n' && lixo != EOF);
}

void leString(char mensagem[MAX_STRING], char vetorCaracteres[], int maximoCaracteres)
{
    int tamanhoString;
    do
    {
        printf("%s", mensagem);
        fgets(vetorCaracteres, maximoCaracteres, stdin);
        tamanhoString = strlen(vetorCaracteres);
        if(vetorCaracteres[tamanhoString-1] != '\n')
        {
            clearStdIn();
        }
        else
        {
            vetorCaracteres[tamanhoString-1] ='\0';
        }
        if (tamanhoString == 1)
        {
            printf("Nao foram introduzidos caracteres!!! . apenas carregou no ENTER \n\n");
        }
    }
    while(tamanhoString == 1);
}

int leInteiro(char mensagem[MAX_STRING],int minimo, int maximo)
{
    int controlo, x;
    do{
        printf("%s [%d;%d]: ", mensagem, minimo, maximo);
        controlo = scanf("%d", &x);
        clearStdIn();
    }while (maximo < x || minimo > x || controlo == 0);
    return x;
}

tipoData leData()
{
    int maxDia = 0, minDia = 1;
    tipoData data;
    data.dia=0;
    data.mes=0;
    data.ano=0;
    data.ano = leInteiro(("Ano"), LERDATA_ANO_MIN,LERDATA_ANO_MAX);
    data.mes = leInteiro("Mes", 1, 12);
    switch(data.mes)
    {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            maxDia=31;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            maxDia=30;
            break;
        case 2:
            if((data.ano % 4 == 0 && data.ano % 100 != 0) || data.ano % 400 == 0)
            {
                maxDia=29;
            }
            else
            {
                maxDia=28;
            }
            break;
    }/*switch data*/
    data.dia = leInteiro("Dia", minDia, maxDia);
    return data;
}

int verificaDatas(tipoData inicio, tipoData fim)
{
    /*
    return -1 - invalidas
    return  0 - iguais
    return  1 - valida fim maior
    */
    int varReturn=1;
    if((inicio.ano>fim.ano) || (inicio.ano==fim.ano && inicio.mes>fim.mes) || (inicio.ano==fim.ano && inicio.mes==fim.mes && inicio.dia>fim.dia))
    {
        varReturn = -1;
    }else{
        if(inicio.ano==fim.ano && inicio.mes==fim.mes && inicio.dia==fim.dia)
        {
            varReturn = 0;
        }
    }
    return varReturn;
}

tipoHora leHora()
{
    tipoHora hora;
    hora.hora = 0;
    hora.minutos = 0;
    hora.segundos = 0;
    hora.hora = leInteiro("Hora", 0,23);
    hora.minutos = leInteiro("Minuto", 0,59);
    hora.segundos = leInteiro("Segundo", 0,59);
    return hora;
}

int verificaHoras(tipoHora inicio, tipoHora fim)
{
    /*
    return -1 - invalidas
    return  0 - iguais
    return  1 - valida fim maior
    */
    int varReturn=1;
    if((inicio.hora>fim.hora) || (inicio.hora==fim.hora && inicio.minutos>fim.minutos) || (inicio.hora==fim.hora && inicio.minutos==fim.minutos && inicio.segundos>fim.segundos))
    {
        varReturn = -1;
    }else{
        if(inicio.hora==fim.hora && inicio.minutos==fim.minutos && inicio.segundos==fim.segundos)
        {
            varReturn = 0;
        }
    }
    return varReturn;
}

tipoPontoTemporal lePontoTemporal(char mensagem[MAX_STRING])
{
    tipoPontoTemporal tpt;
    printf("%s\n", mensagem);
    tpt.hora = leHora();
    tpt.data = leData();
    return tpt;
}

int verificaPontoTemporal(tipoPontoTemporal inicio, tipoPontoTemporal fim)
{
    /*
    return -1 - invalidas
    return  0 - iguais
    return  1 - valida fim maior
    */
    int datas, horas, varReturn=1;
    datas = verificaDatas(inicio.data, fim.data);
    horas = verificaHoras(inicio.hora, fim.hora);
    if(datas==-1 || (datas==0 && horas==-1))
    {
        varReturn = -1;
    }else{
        if(datas==0 && horas==0)
        {
            varReturn = 0;
        }
    }
    return varReturn;
}

