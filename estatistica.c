#include "estatistica.h"

void calcularDistanciaMediaPercorridaPorCadaBicicleta(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, tipoBicicleta Bicicletas[], int quantidadeBicicletas)
{
    int i, pos;
    int *emprestimosPorBicicleta;
    emprestimosPorBicicleta = malloc(quantidadeBicicletas * sizeof(int));
    for(i=0;i<quantidadeBicicletas;i++)
    {
        emprestimosPorBicicleta[i]=0;
        Bicicletas[i].distanciaMediaPercorrida=0;
    }
    for(i=0;i<quantidadeEmprestimos;i++)
    {
        if(Emprestimos[i].finalEmprestimo.hora.hora!=-1)
        {
            pos = procuraBicicleta(Bicicletas, quantidadeBicicletas, Emprestimos[i].designacao);
            Bicicletas[pos].distanciaMediaPercorrida += Emprestimos[i].distanciaPercorrida;
            emprestimosPorBicicleta[pos]++;
        }
    }
    for(i=0;i<quantidadeBicicletas;i++)
    {
        if(emprestimosPorBicicleta[i]!=0)
        {
            Bicicletas[i].distanciaMediaPercorrida /= emprestimosPorBicicleta[i];
        }
    }
    free(emprestimosPorBicicleta);
}

void mostrarDistanciaMediaPercorridaPorBicicleta(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, tipoBicicleta Bicicletas[], int quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS], char estado[][MAX_STRING_GLOBAIS])
{
    int i;
    calcularDistanciaMediaPercorridaPorCadaBicicleta(Emprestimos, quantidadeEmprestimos, Bicicletas, quantidadeBicicletas);
    for(i=0;i<quantidadeBicicletas;i++)
    {
        mostraBicicleta(Bicicletas[i], localizacao, estado);
        printf("Distancia media percorrida: %.2f metros\n\n\n\n", Bicicletas[i].distanciaMediaPercorrida);
    }
}

int emprestimosEntreData(tipoData inicio, tipoData fim, tipoEmprestimo Emprestimos[], int quantidadeEmprestimos)
{
    /* em caso de data errada return -1 */
    int i, quant=0;
    if(verificaDatas(inicio, fim)<0)
    {
        printf("\nData invalida\n");
        return -1;
    }
    for(i=0;i<quantidadeEmprestimos;i++)
    {
        if(Emprestimos[i].inicioEmprestimo.data.ano >= inicio.ano && Emprestimos[i].inicioEmprestimo.data.mes >= inicio.mes && Emprestimos[i].inicioEmprestimo.data.dia >= inicio.dia && Emprestimos[i].finalEmprestimo.data.ano <= fim.ano && Emprestimos[i].finalEmprestimo.data.mes <= fim.mes && Emprestimos[i].finalEmprestimo.data.dia <= fim.dia)
        {
            quant++;
        }
    }
    return quant;
}

float *percentagemEmprestimosPorTipo(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, int quantidadeTipos, tipoUtente Utentes[], int quantidadeUtentes)
{
    int i, pos;
    float *percentagens;
    percentagens = malloc(quantidadeTipos * sizeof(float));
    for(i=0;i<quantidadeEmprestimos;i++)
    {
        pos = procurarUtente(Utentes, quantidadeUtentes, Emprestimos[i].codigoUtente);
        percentagens[Utentes[pos].codigoTipo]++;
    }
    for(i=0;i<quantidadeTipos;i++)
    {
        percentagens[i] /= quantidadeEmprestimos;
        percentagens[i] *= 100;
    }
    return percentagens;
}

void mostrarPercentagensPorTipo(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, tipoUtente Utentes[], int quantidadeUtentes, char utente_tipo[][MAX_STRING_GLOBAIS], int quantidadeTipos)
{
    float *percentagens;
    int i;
    percentagens = percentagemEmprestimosPorTipo(Emprestimos, quantidadeEmprestimos, quantidadeTipos, Utentes, quantidadeUtentes);
    printf("\n\nPercentagem de emprestimos efetuados por cada tipo de utente\n");
    for(i=0;i<quantidadeTipos;i++)
    {
        printf("%s - %.2f \n", utente_tipo[i], percentagens[i]);
    }
    printf("\n\n");
    free(percentagens);
}

void ordenaUtentesPorMaisEmprestimosDecres(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, tipoUtente Utentes[], int quantidadeUtentes, char utente_tipo[][MAX_STRING_GLOBAIS])
{
    int x, i, varInteiro;
    int *quantEmprestimos;
    int *ordemUtentes;
    ordemUtentes = malloc(sizeof(int)*quantidadeUtentes);
    quantEmprestimos = malloc(sizeof(int)*quantidadeUtentes);
    for(i=0;i<quantidadeUtentes;i++)
    {
        quantEmprestimos[i]=0;
        ordemUtentes[i]=i;
    }
    for(i=0;i<quantidadeEmprestimos;i++)
    {
        quantEmprestimos[procurarUtente(Utentes, quantidadeUtentes, Emprestimos[i].codigoUtente)]++;
    }
    for(x=0;x<quantidadeUtentes;x++)
    {
        for(i=0;i<quantidadeUtentes-1;i++)
        {
            if(quantEmprestimos[i]<quantEmprestimos[i+1])
            {
                varInteiro = quantEmprestimos[i];
                quantEmprestimos[i] = quantEmprestimos[i+1];
                quantEmprestimos[i+1] = varInteiro;
                varInteiro = ordemUtentes[i];
                ordemUtentes[i] = ordemUtentes[i+1];
                ordemUtentes[i+1] = varInteiro;
            }
        }
    }
    for(i=0;i<quantidadeUtentes;i++)
    {
        mostraUtente(Utentes[ordemUtentes[i]], utente_tipo);
        printf("Quantidade emprestimos: %d\n\n\n", quantEmprestimos[i]);
    }
    free(ordemUtentes);
    free(quantEmprestimos);
}

void mostraCampusComMaiorQuantiadeDeEmprestimos(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, char localizacao[][MAX_STRING_GLOBAIS])
{
    int x, i, campusQuant[LOCALIZACAO_BICICLETA_COUNT], ordemCampus[LOCALIZACAO_BICICLETA_COUNT];
    int varInteiro=0;
    for(i=0;i<LOCALIZACAO_BICICLETA_COUNT;i++)
    {
        campusQuant[i]=0;
        ordemCampus[i]=i;
    }
    for(i=0;i<quantidadeEmprestimos;i++)
    {
        campusQuant[Emprestimos[i].codigoLocalizacaoOrigem]++;
    }
    for(x=0;x<LOCALIZACAO_BICICLETA_COUNT;x++)
    {
        for(i=0;i<LOCALIZACAO_BICICLETA_COUNT-1;i++)
        {
            if(campusQuant[i]<campusQuant[i+1])
            {
                varInteiro = ordemCampus[i];
                ordemCampus[i] = ordemCampus[i+1];
                ordemCampus[i+1] = varInteiro;
                varInteiro = campusQuant[i];
                campusQuant[i] = campusQuant[i+1];
                campusQuant[i+1] = varInteiro;
            }
        }
    }
    for(i=0;i<LOCALIZACAO_BICICLETA_COUNT;i++)
    {
        if(campusQuant[0]==campusQuant[i])
        {
            printf("\nCampus: %s\tQuantidade de emprestimos: %d\n\n", localizacao[ordemCampus[i]], campusQuant[i]);
        }
    }
}

int quantidadeUtentesPorBicicleta(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, char designacao[MAX_STRING_DESIGNACAO])
{
    int x, i, quant=0;
    int codigoUtente[MAX_UTENTE];
    int jaExiste=0;
    for(x=0;x<MAX_UTENTE;x++)
    {
        codigoUtente[x]=-1;
    }
    for(x=0;x<quantidadeEmprestimos;x++)
    {
        if(strcmp(designacao, Emprestimos[x].designacao)==0)
        {
            for(i=0;i<MAX_UTENTE;i++)
            {
                if(Emprestimos[x].codigoUtente==codigoUtente[i])
                {
                    jaExiste=1;
                }else{
                    jaExiste=0;
                }
            }
            if(jaExiste!=1)
            {
                codigoUtente[quant]=Emprestimos[x].codigoUtente;
                quant++;
            }
        }
    }
    return quant;
}

void mostrarNumeroDeUtentesQueUtilizaramUmaBicicleta(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, tipoBicicleta Bicicletas[], int quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS], char estado_bicicleta[][MAX_STRING_GLOBAIS])
{
    int varInteiro, quantidadeUtilizadores=0;
    char designacaoBicicleta[MAX_STRING_DESIGNACAO];
    do{
        mostraBicicletas(Bicicletas, quantidadeBicicletas, localizacao, estado_bicicleta);
        varInteiro = leDesignacao("Quantidade de utilizadores diferentes que utilizaram uma bicicleta" ,designacaoBicicleta, Bicicletas, quantidadeBicicletas);
    }while(varInteiro<0 && varInteiro!=-2);
    if(varInteiro!=-2)
    {
        mostraBicicleta(Bicicletas[varInteiro], localizacao, estado_bicicleta);
        quantidadeUtilizadores = quantidadeUtentesPorBicicleta(Emprestimos, quantidadeEmprestimos, designacaoBicicleta);
        printf("Foi utilizada por %d ", quantidadeUtilizadores);
        if(quantidadeUtilizadores==1)
        {
            printf("utente\n\n\n");
        }else{
            printf("utentes\n\n\n");
        }
    }
}

void quantidadeDeEmprestimosEntreDuasDatas(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos)
{
    tipoData dataX, dataY;
    int varInteiro;
    printf("\nIntroduza o intervalo de que em que pretende saber a quantidade de emprestimos efetuados:\n\n");
    dataX = leData();
    dataY = leData();
    if(verificaDatas(dataX, dataY)!=-1)
    {
        varInteiro = emprestimosEntreData(dataX, dataY, Emprestimos, quantidadeEmprestimos);
        printf("\nEntre %d/%d/%d e %d/%d/%d", dataX.dia, dataX.mes, dataX.ano, dataY.dia, dataY.mes, dataY.ano);
    }else{
        varInteiro = emprestimosEntreData(dataY, dataX, Emprestimos, quantidadeEmprestimos);
        printf("\nEntre %d/%d/%d e %d/%d/%d", dataY.dia, dataY.mes, dataY.ano, dataX.dia, dataX.mes, dataX.ano);
    }
    if(varInteiro!=1)
    {
        printf(" ocorreram %d emprestimos\n\n", varInteiro);
    }else{
        printf(" ocorreu 1 emprestimo\n\n");
    }
}

void ultimaBicicleta_MostraEmprestimos(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, tipoUtente Utentes[], int quantidadeUtentes, tipoBicicleta Bicicletas[], int quantidadeBicicletas, char utente_tipo[][MAX_STRING_GLOBAIS], char localizacao[][MAX_STRING_GLOBAIS], char estado_bicicleta[][MAX_STRING_GLOBAIS])
{
    int posUtente, posEmprestimo, posBicicleta, codigoUtente;
    if(quantidadeUtentes>0)
    {
        mostraUtentes(Utentes, quantidadeUtentes, utente_tipo);
        do{
            codigoUtente = leInteiro("\nIntroduza o codigo do utente de que pretende saber a ultima bicicleta utilizada\n(0 para voltar atras)\n> ",0,50);
            posUtente = procurarUtente(Utentes, quantidadeUtentes, codigoUtente);
        }while(posUtente==-1 && codigoUtente!=0);
        if(codigoUtente!=0)
        {
            if(Utentes[posUtente].ultimoNumeroEmprestimo!=-1)
            {
                posEmprestimo = procuraEmprestimo(Emprestimos, quantidadeEmprestimos, Utentes[posUtente].ultimoNumeroEmprestimo);
                printf("\n\nA ultima bicicleta utilizada por %s foi: ", Utentes[posUtente].nome);
                posBicicleta = procuraBicicleta(Bicicletas, quantidadeBicicletas, Emprestimos[posEmprestimo].designacao);
                if(posBicicleta!=-1)
                {
                    mostraBicicleta(Bicicletas[posBicicleta], localizacao, estado_bicicleta);
                    printf("\nEmprestimos de %s\n", Utentes[posUtente].nome);
                    mostrarEmprestimosUtente(1, Emprestimos, quantidadeEmprestimos, codigoUtente, localizacao);
                }else{
                    printf("\nA bicicleta nao foi encontrada\n");
                }
            }else{
                printf("O utente escolhido ainda nao terminou nenhum emprestimo\n");
            }
        }else{
            printf("\nAcao cancelada\n");
        }
    }else{
        printf("Ainda nao registou utentes\n\n");
    }
}
