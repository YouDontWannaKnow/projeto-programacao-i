#ifndef EMPRESTIMOS_H_INCLUDED
#define EMPRESTIMOS_H_INCLUDED
#include "camada1.h"

tipoEmprestimo *alojaEmprestimos(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos);

/* CRIACAO EMPRESTIMO */
int novoEmprestimo(tipoUtente utente, tipoEmprestimo *Emprestimos[], int *quantidadeEmprestimos, tipoEmprestimo *ListaDeEspera[], int *quantidadeListaDeEspera, tipoBicicleta Bicicletas[], int quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS], char estado_bicicleta[][MAX_STRING_GLOBAIS]);
int criarEmprestimo(tipoEmprestimo *EmprestimoEmCurso, int codigoUtente, tipoBicicleta Bicicletas[], int quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS], char estado[][MAX_STRING_GLOBAIS]);
tipoEmprestimo introducaoDadosIndependentesEmprestimo(int codigoUtente, char localizacao[][MAX_STRING_GLOBAIS]);
tipoEmprestimo introducaoDadosDependentesEmprestimo(tipoEmprestimo emprestimo, tipoBicicleta *bicicleta, tipoPontoTemporal inicioEmprestimo);

int novoNumeroEmprestimo(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos);
int novoNumeroEmprestimoEspera(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos);


/* FINALIZA EMPRESTIMO */
int finalizacaoEmprestimo(tipoUtente *utente, tipoEmprestimo *Emprestimos[], int *quantidadeEmprestimos, tipoEmprestimo *ListaDeEspera[], int *quantidadeListaDeEspera, tipoBicicleta Bicicletas[], int quantidadeBicicletas, char localizacao[][MAX_STRING_GLOBAIS], char estado[][MAX_STRING_GLOBAIS]);


/* ELIMINA EMPRESTIMO */
tipoPontoTemporal removeEmprestimo(int lerPontoTemp, int numeroEmprestimo, int estado_bicicleta, tipoEmprestimo *Emprestimos[], int *quantidadeEmprestimos, tipoBicicleta Bicicletas[], int quantidadeBicicletas);


/* PROCURA EMPRESTIMOS */
int procuraEmprestimo(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, int numeroEmprestimo);
int procuraEmprestimoADecorrer(tipoEmprestimo Emprestimos[], int quantidade, int codigoUtente);
int procurarUltimoEmprestimoUtente(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, int codigoUtente);


/* CALCULOS */
int distanciaTotalTodosEmprestimos(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos);
int distanciaTotalUtente(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, int codigoUtente);
int distanciaTotalBicicleta(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, char designacaoBicicleta[MAX_STRING_DESIGNACAO]);


/* MOSTRAR */
void mostrarEmprestimo(tipoEmprestimo emprestimo, char localizacao[][MAX_STRING_GLOBAIS]);
void mostrarEmprestimos(tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, char localizacao[][MAX_STRING_GLOBAIS]);
int mostrarEmprestimosUtente(int mostrar, tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, int codigoUtente, char localizacao[][MAX_STRING_GLOBAIS]);
int mostrarEmprestimosBicicleta(int mostrar, tipoEmprestimo Emprestimos[], int quantidadeEmprestimos, char designacao[MAX_STRING_DESIGNACAO], char localizacao[][MAX_STRING_GLOBAIS]);


/* FICHEIROS */
int guardarEmprestimosBin(char stringFicheiroEmprestimos[MAX_STRING], tipoEmprestimo *Emprestimos, int quantidadeEmprestimos);
int carregarEmprestimosBin(char stringFicheiroEmprestimos[MAX_STRING], tipoEmprestimo **Emprestimos, int *quantidadeEmprestimos);

/* LISTA DE ESPERA */
int emEsperaLocal(tipoEmprestimo *ListaDeEspera, int quantidadeListaDeEspera, int codigoLocal);

/* INICIAR A PARTIR DA LISTA DE ESPERA */
void iniciarEmEspera(int lerPontoTemp, tipoPontoTemporal inicio, int numeroEmprestimoEmEspera, char designacao[MAX_STRING_DESIGNACAO], tipoEmprestimo *Emprestimos[], int *quantidadeEmprestimos, tipoEmprestimo *ListaDeEspera[], int *quantidadeListaDeEspera, tipoBicicleta Bicicletas[], int quantidadeBicicletas);
void ordenaListaDeEspera(char tipoOrdem, tipoEmprestimo ListaDeEspera[], int quantidadeListaDeEspera);

#endif /* EMPRESTIMOS_H_INCLUDED */
