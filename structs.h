#ifndef STRUCTS_H_INCLUDED
#define STRUCTS_H_INCLUDED
#include "constants.h"

typedef struct{ /*tipoData */
  int dia, mes, ano;
}tipoData;

typedef struct{ /*tipoHora */
  int hora, minutos, segundos;
}tipoHora;

typedef struct{ /*tipoPontoTemporal */
  tipoData data;
  tipoHora hora;
}tipoPontoTemporal;

typedef struct{ /* tipoBicicleta */
    char designacao[MAX_STRING_DESIGNACAO];
    char modelo[MAX_STRING_MODELO];
    int codigoEstado;
    int codigoLocalizacao;
    int quantidadeAvarias;
    float distanciaMediaPercorrida;
}tipoBicicleta;

typedef struct{ /* tipoUtente */
    int codigoUtente;
    char nome[MAX_STRING_NOME_UTENTE];
    int codigoTipo;
    int telefone;
    int temEmprestimo;
    int ultimoNumeroEmprestimo;
    int penultimoNumeroEmprestimo;
}tipoUtente;

typedef struct{ /* tipoEmprestimo */
    int numeroEmprestimo;
    int codigoUtente;
    char designacao[MAX_STRING_DESIGNACAO];
    tipoPontoTemporal inicioEmprestimo, finalEmprestimo;
    int codigoLocalizacaoOrigem, codigoLocalizacaoDestino;
    int distanciaPercorrida;
    int codigoTipo;
}tipoEmprestimo;

typedef struct{
    tipoPontoTemporal dataAvaria;
    char designacaoBicicleta[MAX_STRING_DESIGNACAO];
    int distanciaPercorrida;
    char descricao[MAX_REPORT_FRASE];
}tipoReportAvaria;


#endif /* STRUCTS_H_INCLUDED */
