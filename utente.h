#ifndef UTENTE_H_INCLUDED
#define UTENTE_H_INCLUDED
#include "camada1.h"

/* CRIAR UTENTE */
void inserirUtente(tipoUtente Utentes[], int *quantidadeUtentes, char utente_tipo[][MAX_STRING_GLOBAIS]);
tipoUtente leUtente(int codigoUtente, char utente_tipo[][MAX_STRING_GLOBAIS]);

int novoCodigoUtente(tipoUtente Utentes[], int quantidadeUtentes);


/* ALTERAR UTENTE */
void alterarUtente(tipoUtente Utentes[], int quantidadeUtentes, char utente_tipo[][MAX_STRING_GLOBAIS]);


/* ELIMINAR UTENTE*/
void removeUtenteReduzido(tipoUtente Utentes[], int *quantidadeUtentes, char utente_tipo[][MAX_STRING_GLOBAIS]);
void removeUtenteVetor(tipoUtente Utentes[], int *quantidadeUtentes, int codigoUtente);


/* MOSTRAR UTENTE */
void mostraUtente(tipoUtente utente, char utente_tipo[][MAX_STRING_GLOBAIS]);
void mostraUtentes(tipoUtente Utentes[], int quantidadeUtentes, char utente_tipo[][MAX_STRING_GLOBAIS]);


/* PROCURAR UTENTE */
int procurarUtente(tipoUtente Utentes[], int quantidadeUtentes, int codigoUtente);


/* FICHEIROS */
int guardarUtentesBin(char stringFicheiroUtentes[MAX_STRING], tipoUtente Utentes[], int quantidadeUtentes);
int carregarUtentesBin(char stringFicheiroUtentes[MAX_STRING], tipoUtente Utentes[], int *quantidadeUtentes);

#endif /* UTENTE_H_INCLUDED */
