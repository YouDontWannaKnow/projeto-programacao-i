#ifndef CAMADA1_H_INCLUDED
#define CAMADA1_H_INCLUDED
#include "bibliotecasSistema.h"
#include "constants.h"
#include "structs.h"
#include "bicicleta.h"
#include "utente.h"
#include "emprestimos.h"
#include "avarias.h"
#include "estatistica.h"

void autoSave(char ficheiroAutoSave[MAX_STRING], int guardar, char opcaoCriterio, tipoEmprestimo *Emprestimos, int quantidadeEmprestimos, tipoEmprestimo *ListaDeEspera, int quantidadeListaDeEspera, tipoUtente *Utentes, int quantidadeUtentes, tipoBicicleta *Bicicletas, int quantidadeBicicletas, int quantidadeAvarias, int distanciaPercorridaTotal);
void autoLoad(char ficheiroAutoSave[MAX_STRING], int *carregar, char *opcaoCriterio, tipoEmprestimo **Emprestimos, int *quantidadeEmprestimos, tipoEmprestimo **ListaDeEspera, int *quantidadeListaDeEspera, tipoUtente *Utentes, int *quantidadeUtentes, tipoBicicleta *Bicicletas, int *quantidadeBicicletas, int *quantidadeAvarias, int *distanciaPercorridaTotal);
int escolherListaStrings(char mensagem[MAX_STRING], char var[][MAX_STRING_GLOBAIS], int inicioLista, int finalLista);
void clearStdIn();
int leInteiro(char mensagem[],int minimo, int maximo);
void leString(char mensagem[MAX_STRING], char vetorCaracteres[MAX_STRING], int maximoCaracteres);
tipoData leData();
int verificaDatas(tipoData inicio, tipoData fim);
tipoHora leHora();
int verificaHoras(tipoHora inicio, tipoHora fim);
tipoPontoTemporal lePontoTemporal(char mensagem[MAX_STRING]);
int verificaPontoTemporal(tipoPontoTemporal inicio, tipoPontoTemporal fim);

#endif /* CAMADA1_H_INCLUDED */
